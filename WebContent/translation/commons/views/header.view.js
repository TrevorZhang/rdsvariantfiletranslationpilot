sap.ui.jsview("translation.commons.views.header", {

	/** Specifies the Controller belonging to this View. 
	* In the case that it is not implemented, or that "null" is returned, this View does not have a Controller.
	* @memberOf translation.commons.header
	*/ 
	getControllerName : function() {
		return "translation.commons.controllers.header";
	},

	/** Is initially called once after the Controller has been instantiated. It is the place where the UI is constructed. 
	* Since the Controller is given to this method, its event handlers can be attached right away. 
	* @memberOf translation.commons.header
	*/ 
	createContent : function(oController) {
		//create the ApplicationHeader control
		var oAppHeader = new sap.ui.commons.ApplicationHeader(this.createId("appHeader")); 
		//configure the welcome area
		oAppHeader.setDisplayWelcome(true);
		//configure the log off area
		oAppHeader.setDisplayLogoff(true);

        return oAppHeader;
	}
});
