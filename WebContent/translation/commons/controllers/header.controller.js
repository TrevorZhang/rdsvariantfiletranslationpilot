sap.ui.controller("translation.commons.controllers.header", {

/**
* Called when a controller is instantiated and its View controls (if available) are already created.
* Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
* @memberOf translation.commons.header
*/
	onInit: function() {
		// configure the branding area
		this.byId("appHeader").setLogoSrc("https://www.sap.com/global/images/SAPLogo.gif");
		this.byId("appHeader").setLogoText("SAP Translation on Cloud");
	}//,

/**
* Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
* (NOT before the first rendering! onInit() is used for that one!).
* @memberOf translation.commons.header
*/
//	onBeforeRendering: function() {
//
//	},

/**
* Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
* This hook is the same one that SAPUI5 controls get after being rendered.
* @memberOf translation.commons.header
*/
//	onAfterRendering: function() {
//
//	},

/**
* Called when the Controller is destroyed. Use this one to free resources and finalize activities.
* @memberOf translation.commons.header
*/
//	onExit: function() {
//
//	}

});