sap.ui.jsview("translation.dialog.views.languagedialog", {

	/** Specifies the Controller belonging to this View. 
	* In the case that it is not implemented, or that "null" is returned, this View does not have a Controller.
	* @memberOf translation.languagedialog
	*/ 
	getControllerName : function() {
		return "translation.dialog.controllers.languagedialog";
	},

	/** Is initially called once after the Controller has been instantiated. It is the place where the UI is constructed. 
	* Since the Controller is given to this method, its event handlers can be attached right away. 
	* @memberOf translation.languagedialog
	*/ 
	createContent : function(oController) {
		jQuery.sap.require("jquery.sap.resources"); 
		
		var oLayout = new sap.ui.commons.layout.MatrixLayout({
			layoutFixed : true,
			columns : 2,
			width : "100%",
			widths : [ "30%", "70%" ]
			});
		
		var targetLabel = new sap.ui.commons.Label({ id : "targetLocationLabel", text : "Target Column"});
		this.targetoBox = new sap.ui.commons.ComboBox("relationPath",{ width: "100%"});
		oLayout.createRow(targetLabel, this.targetoBox);
		
		var languageLabel = new sap.ui.commons.Label({id : "newLanguageLabel", text : "New Language" });
		var languageText = new sap.ui.commons.TextField("addlanguage",{ width: "100%" });
		oLayout.createRow(languageLabel, languageText);
		
		var descriptionLabel = new sap.ui.commons.Label({id : "descriptionLabel", text : "Description"});
		var descriptionText = new sap.ui.commons.TextField("description",{ width: "100%" });
		oLayout.createRow(descriptionLabel, descriptionText);
		
		var referenceLabel = new sap.ui.commons.Label({id : "referenceLabel", text : "Reference"});
		var referenceText = new sap.ui.commons.TextField("reference",{ width: "100%" });
		oLayout.createRow(referenceLabel, referenceText);
		
        return oLayout;  
	},
});
