sap.ui.jsview("translation.dialog.views.translationdialog", {

	/** Specifies the Controller belonging to this View. 
	* In the case that it is not implemented, or that "null" is returned, this View does not have a Controller.
	* @memberOf translation.translationdialog
	*/ 
	getControllerName : function() {
		return "translation.dialog.controllers.translationdialog";
	},

	/** Is initially called once after the Controller has been instantiated. It is the place where the UI is constructed. 
	* Since the Controller is given to this method, its event handlers can be attached right away. 
	* @memberOf translation.translationdialog
	*/ 
	createContent : function(oController) {
		jQuery.sap.require("jquery.sap.resources"); 
		
		var oLayout = new sap.ui.commons.layout.MatrixLayout({
			layoutFixed : true,
			columns : 4,
			width : "100%",
			widths : [ "20%", "40%", "40%" ]
			});
		
		var fromLabel = new sap.ui.commons.Label({id : "from", text : "From" });
		this.fromCombox = new sap.ui.commons.ComboBox("fromCombox",{ width: "100%"});
		this.targetComBox = new sap.ui.commons.ComboBox("relationCombox",{ width: "100%"});
		oLayout.createRow(fromLabel, this.fromCombox, this.targetComBox);
		
		var toLabel = new sap.ui.commons.Label({id : "To", text : "To"});
		this.toCombox = new sap.ui.commons.ComboBox("toCombox",{ width: "100%"});
		this.sourceBox = new sap.ui.commons.ComboBox("sourceCombox",{ width: "100%"});
		oLayout.createRow(toLabel, this.toCombox, this.sourceBox );
		
        return oLayout;  
	}

});
