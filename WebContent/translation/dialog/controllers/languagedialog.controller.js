sap.ui.controller("translation.dialog.controllers.languagedialog", {

/**
* Called when a controller is instantiated and its View controls (if available) are already created.
* Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
* @memberOf translation.languagedialog
*/
//	onInit: function() {
//
//	},

/**
* Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
* (NOT before the first rendering! onInit() is used for that one!).
* @memberOf translation.languagedialog
*/
	onBeforeRendering: function() {
		var treeModel = new sap.ui.model.json.JSONModel();
		var hideText = sap.ui.getCore().byId("selectDocumentId");
		jQuery.ajax({
		    url: "servlet/getServices?action=headers&documentId="+ hideText.getValue(),  // for different servers cross-domain restrictions need to be handled
		    contentType: "application/x-www-form-urlencoded; charset=UTF-8",
		    async:false,
		    dataType: "json",
		    success: function(data, textStatus, jqXHR) { // callback called when data is received
		    	treeModel.setData(data);
		    	var oComboBox3 = sap.ui.getCore().byId("relationPath");
		    	oComboBox3.setModel(treeModel);
		    	
		    	var oItemTemplate = new sap.ui.core.ListItem();
		    	oItemTemplate.bindProperty("key", "headerName");  
		    	oItemTemplate.bindProperty("text", "headerName");  
		    	oItemTemplate.bindProperty("enabled", "enabled");  
		    	oComboBox3.bindItems("/", oItemTemplate);  
		    },
		    error: function(jqXHR, textStatus, errorThrown) {
		        alert("Error occurred:" + errorThrown);
		    }
		});
	},
	initialComboxValue: function(defaultSelectedKey) {
		var relationPath = sap.ui.getCore().byId("relationPath");
		relationPath.setSelectedKey(defaultSelectedKey);
	},

/**
* Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
* This hook is the same one that SAPUI5 controls get after being rendered.
* @memberOf translation.languagedialog
*/
//	onAfterRendering: function() {
//
//	},

/**
* Called when the Controller is destroyed. Use this one to free resources and finalize activities.
* @memberOf translation.languagedialog
*/
//	onExit: function() {
//
//	}

});