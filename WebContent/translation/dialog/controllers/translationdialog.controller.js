sap.ui.controller("translation.dialog.controllers.translationdialog", {

/**
* Called when a controller is instantiated and its View controls (if available) are already created.
* Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
* @memberOf translation.translationdialog
*/
//	onInit: function() {
//
//	},

/**
* Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
* (NOT before the first rendering! onInit() is used for that one!).
* @memberOf translation.translationdialog
*/
	onBeforeRendering: function() {
		var comboxModel 		= new sap.ui.model.json.JSONModel();
		var translationModel 	= new sap.ui.model.json.JSONModel();
		var needTransModel 		= new sap.ui.model.json.JSONModel();
		var selectedDocumentId 	= sap.ui.getCore().byId("selectDocumentId");
		jQuery.ajax({
		    url: "servlet/getServices?action=translationDlg&documentId="+ selectedDocumentId.getValue(),  // for different servers cross-domain restrictions need to be handled
		    contentType: "application/x-www-form-urlencoded; charset=UTF-8",
		    async:false,
		    dataType: "json",
		    success: function(data, textStatus, jqXHR) { // callback called when data is received
		    	var relatives = eval(data.relatives);
		    	var needTrans = eval(data.needToTransColumn);
		    	comboxModel.setData(relatives);
		    	needTransModel.setData(needTrans);
		    	var relationCombox 	= sap.ui.getCore().byId("relationCombox");
		    	var sourceCombox 	= sap.ui.getCore().byId("sourceCombox");
		    	relationCombox.setModel(comboxModel);
		    	sourceCombox.setModel(needTransModel);
		    	
		    	var oItemTemplate = new sap.ui.core.ListItem();
		    	oItemTemplate.bindProperty("key", "headerName");  
		    	oItemTemplate.bindProperty("text", "headerName");  
		    	oItemTemplate.bindProperty("enabled", "enabled");  
		    	relationCombox.bindItems("/", oItemTemplate);
		    	sourceCombox.bindItems("/", oItemTemplate);
		    	
		    	// ==================add languages into combox===========
		    	var languages=eval(data.languages);
		    	translationModel.setData(languages);
		    	var fromComboBox 	= sap.ui.getCore().byId("fromCombox");
		    	var toComboBox 		= sap.ui.getCore().byId("toCombox");
		    	fromComboBox.setModel(translationModel);
		    	toComboBox.setModel(translationModel);
		    	
		    	var translationItemTemplate = new sap.ui.core.ListItem();
		    	translationItemTemplate.bindProperty("key", "language");  
		    	translationItemTemplate.bindProperty("text", "language");  
		    	translationItemTemplate.bindProperty("enabled", "enabled");  
		    	fromComboBox.bindItems("/", translationItemTemplate);
		    	toComboBox.bindItems("/", translationItemTemplate);
		    },
		    error: function(jqXHR, textStatus, errorThrown) {
		        alert("Error occurred:" + errorThrown);
		    }
		});
	},
/**
* Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
* This hook is the same one that SAPUI5 controls get after being rendered.
* @memberOf translation.translationdialog
*/
//	onAfterRendering: function() {
//
//	},

/**
* Called when the Controller is destroyed. Use this one to free resources and finalize activities.
* @memberOf translation.translationdialog
*/
//	onExit: function() {
//
//	}

});