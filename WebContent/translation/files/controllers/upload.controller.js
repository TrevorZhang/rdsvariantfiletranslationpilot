sap.ui.controller("translation.files.controllers.upload", {

/**
* Called when a controller is instantiated and its View controls (if available) are already created.
* Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
* @memberOf translation.files.upload
*/
//	onInit: function() {
//
//	},

/**
* Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
* (NOT before the first rendering! onInit() is used for that one!).
* @memberOf translation.files.upload
*/
//	onBeforeRendering: function() {
//
//	},

/**
* Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
* This hook is the same one that SAPUI5 controls get after being rendered.
* @memberOf translation.files.upload
*/
//	onAfterRendering: function() {
//
//	},

/**
* Called when the Controller is destroyed. Use this one to free resources and finalize activities.
* @memberOf translation.files.upload
*/
//	onExit: function() {
//
//	}
	doFileUpload : function(oEvent) {
		var url = "servlet/Upload";
		var fileLoader = sap.ui.getCore().byId("FileLoader");
		var fileName = fileLoader.getValue();
		if (fileName == "") {
			jQuery.sap.require("sap.ui.commons.MessageBox");
			sap.ui.commons.MessageBox.show("Please choose File.", sap.ui.commons.MessageBox.Icon.INFORMATION, "Information");
		} else {
			url = url + "?file_name=" + fileName;
			fileLoader.setUploadUrl(url);
			fileLoader.upload();
		}
	},
	doFileLoadComplete : function(oEvent) {
		jQuery.sap.require("sap.ui.commons.MessageBox");
		sap.ui.commons.MessageBox.show("The file upload have been successed.", sap.ui.commons.MessageBox.Icon.INFORMATION, "Information");
		
		sap.ui.controller("translation.files.controllers.trans").queryTreeData("");
	}
});