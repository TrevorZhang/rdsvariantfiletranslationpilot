sap.ui.controller("translation.files.controllers.trans", {

	/**
	* Called when a controller is instantiated and its View controls (if available) are already created.
	* Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
	* @memberOf translation.trans
	*/
	onInit: function() {
		this.bindTreeNode();
		this.bindColumnsEvent();
	},
	bindTreeNode: function() {
		var oTree 	= sap.ui.getCore().byId("tree");
		var oModel 	= new sap.ui.model.json.JSONModel();
	    oTree.setModel(oModel);
		var oTreeNode = function (sId, oContext){
			return new sap.ui.commons.TreeNode(sId)
			.bindProperty("text", oContext.sPath + "/displayName")
			.bindProperty("icon", oContext.sPath + "/icon")
			.addCustomData(new sap.ui.core.CustomData({
				key: "modelId",
				value: oContext.oModel.getProperty(oContext.sPath + "/documentId"),
				writeToDom:true
			})).setExpanded(false);
			
			return oTreeNode;
		};
		oTree.bindAggregation("nodes", "/",oTreeNode );
	},
	bindTableTemplate: function() {
		var oTable = sap.ui.getCore().byId("tableContent");
		// bind columns template and rows template
		var oColumnsTemplate = new sap.ui.table.Column({
			 label : new sap.ui.commons.Label().bindProperty("text", "column", function (aValue) {
				return aValue;
			 }),
			 template : new sap.ui.commons.TextView().bindProperty("text", "column", function(aValue){
				return aValue;
	        })
		}); 
		oTable.bindColumns("/columns", oColumnsTemplate);
		oTable.bindRows("/Customers");
	},
	/**
	 * Get column selected value for detail content table
	 */
	bindColumnsEvent: function() {
		var oTable = sap.ui.getCore().byId("tableContent");
		this.initialDialog();
		var that = this;
		oTable.attachColumnSelect(function(oEvent){
			var columnSelected	= oEvent.getParameter("column").getLabel().getText();
			var languageControl = sap.ui.controller("translation.dialog.controllers.languagedialog");
			languageControl.initialComboxValue(columnSelected);
			that.oDialog.open();
        });
	},
	/**
	 * Initial new language dialog
	 */
	initialDialog: function() {
		var that = this;
		var languageDialogView = sap.ui.view({
			type:sap.ui.core.mvc.ViewType.JS, 
			viewName:"translation.dialog.views.languagedialog"
		});
		
		var exeOBtn = new sap.ui.commons.Button({
			text : "Execute",
			enabled : true,
			press : function() {
				var documentId 		= sap.ui.getCore().byId("selectDocumentId").getValue();
				var newLanguage 	= sap.ui.getCore().byId("addlanguage").getValue();
				var description 	= sap.ui.getCore().byId("description").getValue();
				var reference 		= sap.ui.getCore().byId("reference").getValue();
				var relativeColumn 	= languageDialogView.targetoBox.getValue();
				that.addAdditionColumn(relativeColumn, newLanguage, documentId, description, reference);
			}
		});
		
		this.oDialog = new sap.ui.commons.Dialog({
			modal : true,
			title : "Additional Language",
			minWidth:"400px",
			minHeight:"200px",
			maxWidth:"400px",
			maxHeight:"200px",
			buttons : [ exeOBtn ],
			content : [ languageDialogView ]
		});
	},
	/**
	* Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
	* (NOT before the first rendering! onInit() is used for that one!).
	* @memberOf translation.trans
	*/
	onBeforeRendering: function() {
		this.queryTreeData("");
	},
	addAdditionColumn: function(relativePath, newLanguage,documentId, description, reference) {
		var that = this;
		$.ajax({
		    url: "servlet/postServices",
		    type: 'POST',        
		    dataType: 'json',
		    contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
		    data: {
		    	action				: "addLanguage",
		    	documentId			: documentId,
		    	relationLocation	: relativePath,
		    	newLanguage			: newLanguage,
		    	description			: description,
		    	reference			: reference
		    },success: function(data) {
		    	that.getDetailContent(documentId);
		    	that.oDialog.close();// Close language dialog
		    },error: function(jqXHR, textStatus, errorThrown) {
		        alert("Error occurred:" + errorThrown);
		    }
		});
	},
	onclickTranslation: function(fromCombox, fromColumn, toCombox, toColumn, documentId) {
		var that = this;
		$.ajax({
		    url: "servlet/postServices",
		    type: 'POST',        
		    dataType: 'json',
		    contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
		    data: {
		    	action			: "doTranslation",
		    	documentId		: documentId,
		    	fromLang		: fromCombox,
		    	relationLocation: fromColumn,
		    	toLang			: toCombox,
		    	source			: toColumn
		    },success: function(data) {
		    	that.getDetailContent(documentId);
		    	//that.translationDialog.close();// Close language dialog
		    },error: function(jqXHR, textStatus, errorThrown) {
		        alert("Error occurred:" + errorThrown);
		    }
		});
	},
	/**
	* Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
	* This hook is the same one that SAPUI5 controls get after being rendered.
	* @memberOf translation.files.download
	*/
//	onAfterRendering: function() {
//	},
	/**
	* Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
	* This hook is the same one that SAPUI5 controls get after being rendered.
	* @memberOf translation.trans
	*/
	getTreeData: function() {
		var treeModel = new sap.ui.model.json.JSONModel();
		jQuery.ajax({
		    url: "servlet/getServices?action=uploadData",  // for different servers cross-domain restrictions need to be handled
		    contentType: "application/x-www-form-urlencoded; charset=UTF-8",
		    async:false,
		    dataType: "json",
		    success: function(data, textStatus, jqXHR) { // callback called when data is received
		    	treeModel.setData(data);
		    	sap.ui.getCore().byId("tree").setModel(treeModel);
		    },
		    error: function(jqXHR, textStatus, errorThrown) {
		        alert("Error occurred:" + errorThrown);
		    }
		});
	},
	queryTreeData: function(query) {
		var treeModel = new sap.ui.model.json.JSONModel();
		jQuery.ajax({
		    url: "servlet/getServices?action=uploadData&query="+ query,  // for different servers cross-domain restrictions need to be handled
		    contentType: "application/x-www-form-urlencoded; charset=UTF-8",
		    async:false,
		    dataType: "json",
		    success: function(data, textStatus, jqXHR) { // callback called when data is received
		    	treeModel.setData(data);
		    	sap.ui.getCore().byId("tree").setModel(treeModel);
//		    	if(query != "") {		    		
//		    		sap.ui.getCore().byId("tree").expandAll();
//		    	}
		    },
		    error: function(jqXHR, textStatus, errorThrown) {
		        alert("Query data error occurred:" + errorThrown);
		    }
		});
	},
	onTreeClick: function(oControlEvent) {
		var documentId = "";
		var customDataList = oControlEvent.getParameter("node").getCustomData();
		if(customDataList!=null && customDataList.length==1 && customDataList[0].getKey("modelId")){
			documentId = customDataList[0].getValue("modelId");
		}
		this.getDetailContent(documentId);
	},
	getDetailContent: function(documentId) {
		var batchBtn 	= sap.ui.getCore().byId("batchTranslation");
		var downloadBtn = sap.ui.getCore().byId("downloadDocument");
		var transBtn 	= sap.ui.getCore().byId("translation");
		downloadBtn.setEnabled(true);
		
		var oTable 						= sap.ui.getCore().byId("tableContent");
		var contentModel 				= new sap.ui.model.json.JSONModel();
		
		var transoTable 				= sap.ui.getCore().byId("transatedContent");
		var transModel 					= new sap.ui.model.json.JSONModel();
		var currentSelectedDocumentId 	= sap.ui.getCore().byId("selectDocumentId");
		currentSelectedDocumentId.setValue(documentId);
		jQuery.ajax({
			url: "servlet/getServices?action=detail&documentId=" + documentId,  // for different servers cross-domain restrictions need to be handled
			contentType: "application/x-www-form-urlencoded; charset=UTF-8",
			async:false,
			dataType: "json",
			success: function(data, textStatus, jqXHR) { // callback called when data is received
				var type = data.type;
				if (type == "zip"){
			        batchBtn.setEnabled(true);
			        transBtn.setEnabled(false);
			    } else {
			       batchBtn.setEnabled(false);
			       transBtn.setEnabled(true);
			    }
				
				oTable.destroyColumns();
				var detailTableColumns = data.sourceFile.columns;
				detailTableColumns.forEach(function(column){
					oTable.addColumn(new sap.ui.table.Column({
						label: new sap.ui.commons.Label({text: column.column}),
  	              	  	template: new sap.ui.commons.TextView().bindProperty("text", column.column),
  	              	  	sortProperty: column.column,
  	              	  	filterProperty: column.column,
  	              	  	width: "120px",
  	              	  	hAlign: "Center"
  	              	  }));    		  
  	        	});
				var json=eval(data.sourceFile.rows);
				contentModel.setData(json);
				oTable.setModel(contentModel);
				oTable.bindRows("/"); // Fill customer table with customer records
				
				// assign translated content to table
				transoTable.destroyColumns();
				var transDetailTableColumns = data.translatedFile.columns;
				transDetailTableColumns.forEach(function(column){
					transoTable.addColumn(new sap.ui.table.Column({
						label: new sap.ui.commons.Label({text: column.column}),
  	              	  	template: new sap.ui.commons.TextView().bindProperty("text", column.column),
  	              	  	sortProperty: column.column,
  	              	  	filterProperty: column.column,
  	              	  	width: "120px",
  	              	  	hAlign: "Center"
  	              	  }));    		  
  	        	});
				var transJson=eval(data.translatedFile.rows);
				transModel.setData(transJson);
				transoTable.setModel(transModel);
				transoTable.bindRows("/"); // Fill customer table with customer records
			},
			error: function(jqXHR, textStatus, errorThrown) {
				alert("The details error occurred:" + errorThrown);
			}
		});
	},
	translation: function() {
		var documentId 	= sap.ui.getCore().byId("selectDocumentId");
		$.ajax({
		    url: "servlet/startTranslation",
		    type: 'POST',        
		    dataType: 'json',
		    contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
		    data: {
		    	documentId		: documentId.getValue()
		    },success: function(data) {
		    	var type = data.status;
		    	alert(type);
		    	//that.translationDialog.close();// Close language dialog
		    },error: function(jqXHR, textStatus, errorThrown) {
		    	 alert("The translator error occurred:" + errorThrown);
		    }
		});
	},
	downloadDocument: function() {
		var documentId = sap.ui.getCore().byId("selectDocumentId").getValue();
		window.open("servlet/download?action=download&documentId=" + documentId);
	},
	initialTranslationDialog: function() {
		var that = this;
		var languageDialogView = sap.ui.view({
			type:sap.ui.core.mvc.ViewType.JS, 
			viewName:"translation.dialog.views.translationdialog"
		});
		
		var exeOBtn = new sap.ui.commons.Button({
			text : "Translation",
			enabled : true,
			press : function() {
				var fromCombox 		= sap.ui.getCore().byId("fromCombox").getValue();
				var relativeCombox 	= sap.ui.getCore().byId("relationCombox").getValue();
				var toCombox 		= sap.ui.getCore().byId("toCombox").getValue();
				var sourceCombox 	= sap.ui.getCore().byId("sourceCombox").getValue();
				var documentId 		= sap.ui.getCore().byId("selectDocumentId").getValue();
				that.onclickTranslation(fromCombox, relativeCombox, toCombox, sourceCombox, documentId);
			}
		});
		
		this.translationDialog = new sap.ui.commons.Dialog({
			modal : true,
			title : "Translation",
			minWidth:"500px",
			minHeight:"200px",
			maxWidth:"500px",
			maxHeight:"200px",
			buttons : [ exeOBtn ],
			content : [ languageDialogView ]
		});
	},
	singleTranslation:function() {
		this.translationDialog.open();
	}

	/**
	* Called when the Controller is destroyed. Use this one to free resources and finalize activities.
	* @memberOf translation.trans
	*/
//	onExit: function() {
//
//	}

});