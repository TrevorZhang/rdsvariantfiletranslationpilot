sap.ui.jsview("translation.files.views.upload", {

	/** Specifies the Controller belonging to this View. 
	* In the case that it is not implemented, or that "null" is returned, this View does not have a Controller.
	* @memberOf translation.files.upload
	*/ 
	getControllerName : function() {
		return "translation.files.controllers.upload";
	},

	/** Is initially called once after the Controller has been instantiated. It is the place where the UI is constructed. 
	* Since the Controller is given to this method, its event handlers can be attached right away. 
	* @memberOf translation.files.upload
	*/ 
	createContent : function(oController) {
		jQuery.sap.require("jquery.sap.resources");  
        var oPanel 		= new sap.ui.commons.Panel("Panel", {text: "File Upload"});  
 		
 		//Create a matrix layout with 2 columns
 		var oMatrix = new sap.ui.commons.layout.MatrixLayout({layoutFixed: true, width: '300px', columns: 3});
 		oMatrix.setWidths('100px', '230px', '150px');

 		//Create a form
 		var oLabel = new sap.ui.commons.Label({id:"FileLoaderText", text: 'Variant File:'});
 		var oFileUploader = new sap.ui.commons.FileUploader("FileLoader");  
 		oFileUploader.attachUploadComplete(oController.doFileLoadComplete);
 		var oButton = new sap.ui.commons.Button({ id : this.createId("UploadButton"), text : "Upload"});  
 		oButton.attachPress(oController.doFileUpload);
 		oLabel.setLabelFor(oFileUploader);
 		oMatrix.createRow(oLabel, oFileUploader, oButton);
 		
        oPanel.addContent(oMatrix);
         
        return oPanel;  
	}
});
