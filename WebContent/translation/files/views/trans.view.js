sap.ui.jsview("translation.files.views.trans", {

	/**
	 * Specifies the Controller belonging to this View. In the case that it is
	 * not implemented, or that "null" is returned, this View does not have a
	 * Controller.
	 * 
	 * @memberOf translation.trans
	 */
	getControllerName : function() {
		return "translation.files.controllers.trans";
	},

	/**
	 * Is initially called once after the Controller has been instantiated. It
	 * is the place where the UI is constructed. Since the Controller is given
	 * to this method, its event handlers can be attached right away.
	 * 
	 * @memberOf translation.trans
	 */
	createContent : function(oController) {
		jQuery.sap.require("jquery.sap.resources");
		jQuery.sap.require("sap.ui.table.Table");
		
		// create a horizontal Splitter
		var oPanel = new sap.ui.commons.Panel("transPanel", {text : "Translation", showCollapseIcon: false});
		
		var oContentCard = new sap.ui.commons.layout.MatrixLayout();
        var oCell = new sap.ui.commons.layout.MatrixLayoutCell({hAlign:sap.ui.commons.layout.HAlign.Right});
        // create an add-column button
        var batchButton = new sap.ui.commons.Button("batchTranslation",{
			text : "Batch Translation",
			enabled:false,
			style : sap.ui.commons.ButtonStyle.Emph,
			press : function() {
				oController.translation();
			}
		});
		batchButton.setEnabled(false);
		
		var startButton = new sap.ui.commons.Button("translation",{
			text : "Translation",
			enabled:false,
			style : sap.ui.commons.ButtonStyle.Emph,
			press : function() {
				oController.initialTranslationDialog();
				oController.singleTranslation();
			}
		});
		startButton.setEnabled(false);
		
		// create an add-column button
		var downloadButton = new sap.ui.commons.Button("downloadDocument",{
			text : "Download",
			enabled:false,
			style : sap.ui.commons.ButtonStyle.Emph,
			press : function() {
				oController.downloadDocument();
			}
		});
		downloadButton.setEnabled(false);
		
		oCell.addContent(batchButton);
        oCell.addContent(startButton);
        oCell.addContent(downloadButton);
        oContentCard.createRow(oCell);
        oPanel.addContent(oContentCard);
        
		var oSplitterV = new sap.ui.commons.Splitter("contentsplitter");
		oSplitterV.setSplitterOrientation(sap.ui.commons.Orientation.vertical);
		oSplitterV.setSplitterPosition("25%");
        
		oSplitterV.setShowScrollBars(false);
		oPanel.addContent(oSplitterV);
		
		// create the SearchField
		var oSearch = new sap.ui.commons.SearchField("liveSearch", {
			enableListSuggest: true,
            startSuggestion: 0,
			width:"100%",
			suggest: function(oEvent){
				var sVal = oEvent.getParameter("value");
				oController.queryTreeData(sVal);
			},
			search: function(oEvent){
				var sVal = oEvent.getParameter("query");
				oController.queryTreeData(sVal);
			}
		});
		
		// create the Tree control
		var oTree = new sap.ui.commons.Tree("tree", { select: function(oControlEvent){
			oController.onTreeClick(oControlEvent);
		}});
		oTree.setWidth("100%");
		oTree.setHeight("100%");
		oTree.setShowHeader(false);
		oTree.setShowHeaderIcons(false);
		oTree.setShowHorizontalScrollbar(true);
		oSplitterV.addFirstPaneContent(oSearch);
		oSplitterV.addFirstPaneContent(oTree);
		
		var sourcePanel = new sap.ui.commons.Panel("sourceFile", {text : "Source File"});
		var oTable = new sap.ui.table.Table({
			text : "Source File",
			id : "tableContent",
			visibleRowCount : 7,
			firstVisibleRow : 3,
			width:"100%",
			height:"100%",
			selectionMode : sap.ui.table.SelectionMode.Single
		});
		sourcePanel.addContent(oTable);
		
		var translatedPanel = new sap.ui.commons.Panel("translatedFile", {text : "Translated File"});
		var translatedTable = new sap.ui.table.Table({
			id : "transatedContent",
			visibleRowCount : 7,
			firstVisibleRow : 3,
			width:"100%",
			height:"100%",
			selectionMode : sap.ui.table.SelectionMode.Single
		});
		translatedPanel.addContent(translatedTable);
		
		var oInput = new sap.ui.commons.TextField({
	        id: "selectDocumentId",
	        width : '10em',
	        visible:false
		});
		oSplitterV.addSecondPaneContent(sourcePanel);
		oSplitterV.addSecondPaneContent(translatedPanel);
		
		oPanel.addContent(oInput);
		return oPanel;
	}
});
