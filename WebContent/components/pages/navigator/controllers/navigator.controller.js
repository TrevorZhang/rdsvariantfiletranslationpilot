jQuery.sap.require("sap.m.BusyDialog");
jQuery.sap.require("components.utils.Common");
jQuery.sap.require("sap.ui.model.json.JSONModel");
sap.ui.controller("components.pages.navigator.controllers.navigator", {

/**
* Called when a controller is instantiated and its View controls (if available) are already created.
* Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
* @memberOf components.mainpage
*/
//	onInit: function() {
//
//	},

/**
* Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
* (NOT before the first rendering! onInit() is used for that one!).
* @memberOf components.mainpage
*/
//	onBeforeRendering: function() {
//
//	},

/**
* Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
* This hook is the same one that SAPUI5 controls get after being rendered.
* @memberOf components.mainpage
*/
//	onAfterRendering: function() {
//
//	},

/**
* Called when the Controller is destroyed. Use this one to free resources and finalize activities.
* @memberOf components.mainpage
*/
	extendItemlayout : function() {
		sap.ui.core.Control.extend("ItemLayout", {
			metadata : {
				aggregations : {
					"link" : { type : "sap.ui.commons.Link", multiple : false },
					"image": { type : "sap.ui.commons.Image", multiple : false },
//					"checkBox": { type : "sap.ui.commons.CheckBox", multiple : false},
					"form" : { type : "sap.ui.commons.form.Form", multiple : false },
				}
			},

			renderer : function(rm, ctrl) {
				rm.write("<div");
				rm.writeControlData(ctrl);
				rm.writeAttribute("class", "CustomItemLayout");
				rm.write("><div");
				rm.writeAttribute("class", "CustomItemLayoutInner");
				rm.write("><div");
				rm.writeAttribute("class", "CustomItemLayoutTitle");
				rm.write(">");
				rm.renderControl(ctrl.getImage());
				rm.write("<div>");
				rm.renderControl(ctrl.getLink());
//				rm.write("<div>");
//				rm.renderControl(ctrl.getCheckBox());
				rm.write("</div></div><div");
				rm.writeAttribute("class", "CustomItemLayoutCntnt");
				rm.write(">");
				rm.renderControl(ctrl.getForm());
				rm.write("</div></div></div>");
			},

			onBeforeRendering : function() {
				if (this.resizeTimer) {
					clearTimeout(this.resizeTimer);
					this.resizeTimer = null;
				}
			},

			onAfterRendering : function() {
				var $This = this.$();
				if ($This.parent().parent().hasClass("sapUiUx3DSSVSingleRow")) {
					this._resize();
				} else {
					$This.addClass("CustomItemLayoutSmall");
				}
			},

			_resize : function() {
				if (!this.getDomRef()) {
					return;
				}
				var $This = this.$();
				if ($This.outerWidth() >= 500) {
					$This.removeClass("CustomItemLayoutSmall").addClass("CustomItemLayoutLarge");
				} else {
					$This.removeClass("CustomItemLayoutLarge").addClass("CustomItemLayoutSmall");
				}
				setTimeout(jQuery.proxy(this._resize, this), 300);
			}
		});
	},
	fetchVariantFileData: function(oController) {
		// Initialize the example data and the model
		var busyIndicator = components.utils.Common.BusyIndicatorDialog;
		busyIndicator.open();
		var oModel = new sap.ui.model.json.JSONModel();
		oModel.loadData("servlet/getServices?action=uploadData");
		sap.ui.getCore().setModel(oModel);
    	oModel.attachRequestCompleted(function () {
    		busyIndicator.close();
    	});
	}
});