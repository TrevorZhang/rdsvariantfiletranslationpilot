jQuery.sap.require("sap.m.BusyIndicator");
jQuery.sap.require("sap.ui.ux3.DataSet");
jQuery.sap.require("sap.ui.commons.FileUploader");

jQuery.sap.require("components.utils.Common");
//jQuery.sap.require("sap.m.ButtonType");
//jQuery.sap.require("components.utils.LayoutExtend");

sap.ui.jsview("components.pages.navigator.views.navigator", {

	/**
	 * Specifies the Controller belonging to this View. In the case that it is
	 * not implemented, or that "null" is returned, this View does not have a
	 * Controller.
	 * 
	 * @memberOf components.mainpage
	 */
	getControllerName : function() {
		return "components.pages.navigator.controllers.navigator";
	},
	
	// Initialize the Dataset and the layouts
	createTemplate : function() {
		var c = sap.ui.commons;
		return new ItemLayout({
			link : new c.Link({ text : "{fileName}" }),
			image: new c.Image({ src : "{image}" }),
			form : new c.form.Form({
				width : "100%",
				layout : new c.form.GridLayout(),
				formContainers : [ new c.form.FormContainer({
					formElements : [ 
					 new c.form.FormElement({
						label : new c.Label({ 		text: 	"Reference Language", layoutData : new c.form.GridElementData({ hCells : "10" }) }),
						fields : [ new c.TextField({value: 	"{referenceLanguage}", editable : false }) ]
					}), new c.form.FormElement({
						label : new c.Label({ 		text: 	"Target Language", layoutData : new c.form.GridElementData({ hCells : "10" }) }),
						fields : [ new c.TextField({value: 	"{targetLanguage}", editable : false }) ]
					})
					]
				})]
			})
		});
	},
	/**
	 * Is initially called once after the Controller has been instantiated. It
	 * is the place where the UI is constructed. Since the Controller is given
	 * to this method, its event handlers can be attached right away.
	 * 
	 * @memberOf components.mainpage
	 */
	createContent : function(oController) {
		// Global variables
		this.setHeight("100%");
		oController.extendItemlayout();
//		components.utils.LayoutExtend.itemLayout();
		var oDataSet = new sap.ui.ux3.DataSet({
			items : { path : "/", template: new sap.ui.ux3.DataSetItem({ title : "{fileName}", iconSrc : "{image}", checkable: true})},
			views : [ new sap.ui.ux3.DataSetSimpleView({
				name : "Floating, non-responsive View",
				icon : "./././components/img/tiles.png",
				iconHovered : "./././components/img/tiles2_hover.png",
				iconSelected : "./././components/img/tiles2_hover.png",
				floating : true,
				responsive : false,
				itemMinWidth : 0,
				template : this.createTemplate()
			}), new sap.ui.ux3.DataSetSimpleView({
				name : "Floating, responsive View",
				icon : "./././components/img/tiles.png",
				iconHovered : "./././components/img/tiles_hover.png",
				iconSelected : "./././components/img/tiles_hover.png",
				floating : true,
				responsive : true,
				itemMinWidth : 200,
				template : this.createTemplate()
			}), new sap.ui.ux3.DataSetSimpleView({
				name : "Single Row View",
				icon : "./././components/img/list.png",
				iconHovered : "./././components/img/list_hover.png",
				iconSelected : "./././components/img/list_hover.png",
				floating : false,
				responsive : false,
				itemMinWidth : 0,
				template : this.createTemplate()
			}) ],
			search : function search(oEvent) {
				var sQuery = oEvent.getParameter("query");
				var oBinding = oDataSet.getBinding("items");
				oBinding.filter(!sQuery ? [] : [ new sap.ui.model.Filter("fileName", sap.ui.model.FilterOperator.Contains,sQuery) ]);
				oDataSet.setLeadSelection(-1);
			},
			selectionChanged : function search(oEvent) {
				var busyIndicator = components.utils.Common.BusyIndicatorDialog;
				busyIndicator.open();
				var idx = oEvent.getParameter("newLeadSelectedIndex");
				var variantFile = oDataSet.getItems()[idx];
				if(variantFile != undefined) {
					sap.ui.getCore().getEventBus().publish("nav", "to", { viewName : "components.pages.details.views.variantlist", viewId : "variant_list" , data: variantFile.getTitle()});
					busyIndicator.close();
				}
			}
		});
		
		var oFileUploader = new sap.ui.unified.FileUploader({ name: "variantFile", width: "350px",
			tooltip: "Upload variant file to the HANA Repository.",
			uploadComplete: function (oEvent) {
				oController.fetchVariantFileData(oController);
			}
		});
		
		var submit 	= new sap.ui.commons.Button({text:"Submit", enabled: true, press: function() {
			oFileUploader.setUploadUrl("servlet/Upload?file_name=" + oFileUploader.getValue());
			oFileUploader.upload();
		}});
		oDataSet.addToolbarItem(oFileUploader);
		oDataSet.addToolbarItem(submit);
		
//		var footerBar = components.utils.Common.FooterBar;
		var oPage = new sap.m.Page({
			title : "Ondemand Translator",
			customHeader : new sap.m.Bar({
				contentMiddle: 	[ new sap.m.Label({text: "Ondemand Translator"}) ],
				contentRight:	[ new sap.m.Image({src: "./././components/img/translator.jpg", decorative: true})]
			}),
//			footer : footerBar,
			footer : new sap.m.Bar({
//				contentLeft:  [ new sap.m.Button({text:"Back", type:sap.m.ButtonType.Back, press: [oController.handleNavButtonPress, oController] }) ],
				contentMiddle:[ new sap.m.Image({src: "./././components/img/viz_chart.jpg", decorative: true}) ],
				contentRight: [	new sap.m.Text({ text : "CopyRight@Packaging Framework Team" }) ]
			}),
			content : [ oDataSet ]
		});
		oController.fetchVariantFileData(oController);//fetch data
		return oPage;
	},
});