jQuery.sap.require("components.utils.Common");
sap.ui.controller("components.pages.details.controllers.variantlist", {

/**
* Called when a controller is instantiated and its View controls (if available) are already created.
* Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
* @memberOf components.pages.uploadpage
*/
//	onInit: function() {
//
//	},
	onBeforeShow : function(evt) {  
       this.getView().setBindingContext(evt.data);  
    }, 
	onBack : function(oEvent) {
		var oBindingContext = oEvent.getSource().getBindingContext();
		sap.ui.getCore().getEventBus().publish("nav", "to", {
			viewName: "components.pages.navigator.views.navigator",
			viewId: "Detail" + oEvent.getParameter("id"),
			data: { bindingContext: oBindingContext }
		});
	},
	
	fetchVariantFileData: function(data) {
		// Initialize the busy indicator
		var busyIndicator = components.utils.Common.BusyIndicatorDialog;
		busyIndicator.open();
		var oModel = new sap.ui.model.json.JSONModel();
		oModel.loadData("servlet/getServices?action=variantDetails&documentId=" + data);
		// Initialize the data and the model
    	sap.ui.getCore().setModel(oModel);
    	oModel.attachRequestCompleted(function () {
    		busyIndicator.close();
    	});
	},
	indicatorDialog: function() {
		return new sap.m.BusyDialog({customIcon: "./././components/img/synchronise_48.png"});
	},
	translator: function(pkg) {
		var that = this;
		var busyIndicator = this.indicatorDialog();//components.utils.Common.BusyIndicatorDialog;
		busyIndicator.open();
		$.ajax({
		    url: "servlet/startTranslation",
		    type: 'POST',        
		    dataType: 'json',
		    contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
		    data: {
		    	pkg	: pkg
		    },success: function(data) {
		    	var type = data.status;
		    	that.fetchVariantFileData(pkg);
		    	busyIndicator.close();
		    },error: function(jqXHR, textStatus, errorThrown) {
		    	alert("The translator error occurred:" + errorThrown);
		    	busyIndicator.close();
		    }
		});
	},
	downloadDocument: function(pkg) {
		window.open("servlet/download?action=download&documentId=" + pkg);
	},
	extendItemlayout : function() {
		sap.ui.core.Control.extend("ItemLayout", {
			metadata : {
				aggregations : {
					"link" : 	{ type : "sap.ui.commons.Link", multiple : false },
					"image" : 	{ type : "sap.ui.commons.Image", multiple : false },
					"form" : 	{ type : "sap.ui.commons.form.Form", multiple : false },
				}
			},

			renderer : function(rm, ctrl) {
				rm.write("<div");
				rm.writeControlData(ctrl);
				rm.writeAttribute("class", "CustomItemLayout");
				rm.write("><div");
				rm.writeAttribute("class", "CustomItemLayoutInner");
				rm.write("><div");
				rm.writeAttribute("class", "CustomItemLayoutTitle");
				rm.write(">");
				rm.renderControl(ctrl.getImage());
				rm.write("<div>");
				rm.renderControl(ctrl.getLink());
				rm.write("</div></div><div");
				rm.writeAttribute("class", "CustomItemLayoutCntnt");
				rm.write(">");
				rm.renderControl(ctrl.getForm());
				rm.write("</div></div></div>");
			},

			onBeforeRendering : function() {
				if (this.resizeTimer) {
					clearTimeout(this.resizeTimer);
					this.resizeTimer = null;
				}
			},

			onAfterRendering : function() {
				var $This = this.$();
				if ($This.parent().parent().hasClass("sapUiUx3DSSVSingleRow")) {
					this._resize();
				} else {
					$This.addClass("CustomItemLayoutSmall");
				}
			},

			_resize : function() {
				if (!this.getDomRef()) {
					return;
				}
				var $This = this.$();
				if ($This.outerWidth() >= 500) {
					$This.removeClass("CustomItemLayoutSmall").addClass("CustomItemLayoutLarge");
				} else {
					$This.removeClass("CustomItemLayoutLarge").addClass("CustomItemLayoutSmall");
				}
				setTimeout(jQuery.proxy(this._resize, this), 300);
			}
		});
	},

/**
* Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
* (NOT before the first rendering! onInit() is used for that one!).
* @memberOf components.pages.uploadpage
*/
//	onBeforeRendering: function() {
//
//	},

/**
* Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
* This hook is the same one that SAPUI5 controls get after being rendered.
* @memberOf components.pages.uploadpage
*/
//	onAfterRendering: function() {
//
//	},

/**
* Called when the Controller is destroyed. Use this one to free resources and finalize activities.
* @memberOf components.pages.uploadpage
*/
//	onExit: function() {
//
//	}

});