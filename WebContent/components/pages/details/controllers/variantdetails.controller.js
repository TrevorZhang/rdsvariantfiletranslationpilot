jQuery.sap.require("components.utils.Common");
sap.ui.controller("components.pages.details.controllers.variantdetails", {

/**
* Called when a controller is instantiated and its View controls (if available) are already created.
* Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
* @memberOf components.pages.details.variantdetails
*/
//	onInit: function() {
//
//	},

/**
* Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
* (NOT before the first rendering! onInit() is used for that one!).
* @memberOf components.pages.details.variantdetails
*/
//	onBeforeRendering: function() {
//
//	},

/**
* Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
* This hook is the same one that SAPUI5 controls get after being rendered.
* @memberOf components.pages.details.variantdetails
*/
//	onAfterRendering: function() {
//
//	},

/**
* Called when the Controller is destroyed. Use this one to free resources and finalize activities.
* @memberOf components.pages.details.variantdetails
*/
//	onExit: function() {
//
//	},
	onBeforeShow : function(evt) {  
       this.getView().setBindingContext(evt.data);  
    }, 
	extendItemlayout : function() {
		sap.ui.core.Control.extend("ItemLayout", {
			metadata : {
				aggregations : {
					"link" : 	{ type : "sap.ui.commons.Link", multiple : false },
					"image" : 	{ type : "sap.ui.commons.Image", multiple : false },
					"form" : 	{ type : "sap.ui.commons.form.Form", multiple : false },
				}
			},

			renderer : function(rm, ctrl) {
				rm.write("<div");
				rm.writeControlData(ctrl);
				rm.writeAttribute("class", "CustomItemLayout");
				rm.write("><div");
				rm.writeAttribute("class", "CustomItemLayoutInner");
				rm.write("><div");
				rm.writeAttribute("class", "CustomItemLayoutTitle");
				rm.write(">");
				rm.renderControl(ctrl.getImage());
				rm.write("<div>");
				rm.renderControl(ctrl.getLink());
				rm.write("</div></div><div");
				rm.writeAttribute("class", "CustomItemLayoutCntnt");
				rm.write(">");
				rm.renderControl(ctrl.getForm());
				rm.write("</div></div></div>");
			},

			onBeforeRendering : function() {
				if (this.resizeTimer) {
					clearTimeout(this.resizeTimer);
					this.resizeTimer = null;
				}
			},

			onAfterRendering : function() {
				var $This = this.$();
				if ($This.parent().parent().hasClass("sapUiUx3DSSVSingleRow")) {
					this._resize();
				} else {
					$This.addClass("CustomItemLayoutSmall");
				}
			},

			_resize : function() {
				if (!this.getDomRef()) {
					return;
				}
				var $This = this.$();
				if ($This.outerWidth() >= 500) {
					$This.removeClass("CustomItemLayoutSmall").addClass("CustomItemLayoutLarge");
				} else {
					$This.removeClass("CustomItemLayoutLarge").addClass("CustomItemLayoutSmall");
				}
				setTimeout(jQuery.proxy(this._resize, this), 300);
			}
		});
	},
	onBack : function(data) {
		sap.ui.getCore().getEventBus().publish("nav", "to", {
			viewName: 	"components.pages.details.views.variantlist",
			viewId: 	"Detail" + data.variantPkg,
			data: 		data
		});
	},
	arrayContains: function(array, obj) {
	    var size = array.length;
	    while (size--) {
	       if (array[size] == obj) {
	           return true;
	       }
	    }
	    return false;
	},
	fetchVariantFileData: function(data) {
		var that = this;
		// Initialize the busy indicator
		var busyIndicator = components.utils.Common.BusyIndicatorDialog;
		busyIndicator.open();
		var oTable 		= sap.ui.getCore().byId("details");		
		var transModel 	= new sap.ui.model.json.JSONModel();
		that.targetColumns 		= new Array();
		var referenceColumns 	= new Array();
		jQuery.ajax({
			url: "servlet/getServices?action=detail&pkg="+data.variantPkg + "&documentId="+ data.variantFile,  // for different servers cross-domain restrictions need to be handled
			contentType: "application/x-www-form-urlencoded; charset=UTF-8",
			async:false,
			dataType: "json",
			success: function(oData, textStatus, jqXHR) { // callback called when data is received
				var detailTableColumns = eval(oData.columns);
				oTable.destroyColumns();
//				oTable.destroyRows();
				detailTableColumns.forEach(function(column){
					//Define the columns and the control templates to be used
					var cellTemplate;
					if(column.iTtargetColumn == "true") {
						that.targetColumns.push(column.column);
						cellTemplate = new sap.ui.commons.TextField({
							liveChange : function(oEvent){
								sap.ui.getCore().byId("submit").setEnabled(true);;
							}
						}).bindProperty("value", column.column);
					} else if(column.iReferenceColumn == "true") {
						referenceColumns.push(column.column);
						cellTemplate = new sap.ui.commons.TextView().bindProperty("text", column.column);
					} else {
						cellTemplate = new sap.ui.commons.TextView().bindProperty("text", column.column);
					}
					
					var oColumn = new sap.ui.table.Column({  
					    label: 			new sap.ui.commons.Label({text: column.column}),  
					    template: 		cellTemplate, 
					    sortProperty: 	column.column,  
					    filterProperty: column.column,  
					    width: "120px"  
					});    
					oTable.addColumn(oColumn);
  	        	});
				
				var json=eval(oData.rows);
				transModel.setData(json);
				oTable.setModel(transModel);
				oTable.bindRows("/"); // Fill customer table with customer records
				
				var rowLength = json.length;
				// Set background color for column of table
				oTable.onAfterRendering = function() {
			        if (sap.ui.table.Table.prototype.onAfterRendering) {
			            sap.ui.table.Table.prototype.onAfterRendering.apply(this, arguments);
			        }
			        var cols = this.getColumns();
		        	for (var idx = 0; idx < cols.length; idx++) {
			        	var currColumn = cols[idx];
			        	if(that.arrayContains(that.targetColumns, currColumn.getLabel().getText())) {
		        			var th 		= this.$().find('.sapUiTableColHdr').find('.sapUiTableCol');
			        		$(th[idx]).addClass("columnColor");	// set background color for header
		                    var rows 	= this.$().find('.sapUiTableTr');
		                    for(var rowIdx = 0; rowIdx < rows.length; rowIdx++) {
		                    	if(rowIdx < rowLength) {
		                    		var row = rows[rowIdx];
		                    		var tds = $(row).find('td');
		                    		$(tds[idx + 1]).addClass("columnColor");	// set background color for column
		                    	}
		                    }
			        	}else if(that.arrayContains(referenceColumns, currColumn.getLabel().getText())) {
			        		var th 		= this.$().find('.sapUiTableColHdr').find('.sapUiTableCol');
			        		$(th[idx]).addClass("referenceColumnColor");	// set background color for header
		                    var rows 	= this.$().find('.sapUiTableTr');
		                    for(var rowIdx = 0; rowIdx < rows.length; rowIdx++) {
		                    	if(rowIdx < rowLength) {
		                    		var row = rows[rowIdx];
		                    		var tds = $(row).find('td');
		                    		$(tds[idx + 1]).addClass("referenceColumnColor");	// set background color for column
		                    	}
		                    }
			        	}
		        	}
				}
				transModel.attachRequestCompleted(function () {
		    		busyIndicator.close();
		    	});
			},
			error: function(jqXHR, textStatus, errorThrown) {
				alert("The details error occurred:" + errorThrown);
			}
		});
	},
	updateVariantFile: function() {
		var oTable 		= sap.ui.getCore().byId("details");
		var jsonData 	= "\"" + JSON.stringify(oTable.getModel().getData()) + "\"";
		$.ajax({
		    url: "servlet/update",
		    type: 'POST',        
		    dataType: 'json',
		    contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
		    data: {
		    	updateData	: jsonData
		    },success: function(data) {
		    	
		    },error: function(jqXHR, textStatus, errorThrown) {
		    	
		    }
		});
	}
});