jQuery.sap.require("sap.m.BusyIndicator");
jQuery.sap.require("sap.ui.ux3.DataSet");
jQuery.sap.require("components.utils.Common");
//jQuery.sap.require("components.utils.LayoutExtend");

sap.ui.jsview("components.pages.details.views.variantlist", {
	/**
	 * Specifies the Controller belonging to this View. In the case that it is
	 * not implemented, or that "null" is returned, this View does not have a
	 * Controller.
	 * 
	 * @memberOf components.pages.uploadpage
	 */
	onBeforeShow : function(evt) {  
	    this.getController().onBeforeShow(evt);  
	}, 
	getControllerName : function() {
		return "components.pages.details.controllers.variantlist";
	},
	// Initialize the Dataset and the layouts
	createTemplate : function() {
		var c = sap.ui.commons;
		return new ItemLayout({
			link : new c.Link({ text : "{fileName}" }),
			image: new c.Image({src : "{typeImage}"}),
			form : new c.form.Form({
				width : "100%",
				layout : new c.form.GridLayout(),
				formContainers : [ new c.form.FormContainer({
					formElements : [ 
						new c.form.FormElement({
							label : new c.Label({ 		text: 	"Document Type", layoutData : new c.form.GridElementData({ hCells : "10" }) }),
							fields : [ new c.TextField({value: 	"{objType}", editable : false }) ]
						}),
						new c.form.FormElement({
							label: 	new c.Label({ 		text: 	"Is Translated", layoutData : new c.form.GridElementData({ hCells : "10" })}),
							fields: [ new c.TextField({ value: 	"{translated}", editable : false}) ]
						}),
						new c.form.FormElement({
							label : new c.Label({ 		text: 	"Translated Date", layoutData : new c.form.GridElementData({ hCells : "10" }) }),
							fields : [ new c.TextField({value: 	"{translatedDate}", editable : false }) ]
						}),
						]
				})]
			})
		});
	},
	/**
	 * Is initially called once after the Controller has been instantiated. It
	 * is the place where the UI is constructed. Since the Controller is given
	 * to this method, its event handlers can be attached right away.
	 * 
	 * @memberOf components.pages.uploadpage
	 */
	createContent : function(oController) {
		// Global variables
		this.setHeight("100%");
		var pkg;
		this.addEventDelegate({  
	          onBeforeShow: function(evt) {  
	        	  pkg = evt.data;
	        	  oController.fetchVariantFileData(evt.data);//fetch data  var dataObjectToTransfer = evt.data;  
	    }});
		
		oController.extendItemlayout();
		var oDataSet = new sap.ui.ux3.DataSet({
			items : {
				path : "/",
				template: new sap.ui.ux3.DataSetItem({ title: "{fileName}", iconSrc: "{typeImage}" })},
			views : [ 
						new sap.ui.ux3.DataSetSimpleView({
							name : "Floating, non-responsive View",
							icon : "./././components/img/tiles.png",
							iconHovered : "./././components/img/tiles2_hover.png",
							iconSelected : "./././components/img/tiles2_hover.png",
							floating : true,
							responsive : false,
							itemMinWidth : 0,
							initialItemCount:1000,
							reloadItemCount:1000,
							template : this.createTemplate()
						}), new sap.ui.ux3.DataSetSimpleView({
							name : "Floating, responsive View",
							icon : "./././components/img/tiles.png",
							iconHovered : "./././components/img/tiles_hover.png",
							iconSelected : "./././components/img/tiles_hover.png",
							floating : true,
							responsive : true,
							initialItemCount:1000,
							reloadItemCount:1000,
							itemMinWidth : 200,
							template : this.createTemplate()
						}), new sap.ui.ux3.DataSetSimpleView({
							name : "Single Row View",
							icon : "./././components/img/list.png",
							iconHovered : "./././components/img/list_hover.png",
							iconSelected : "./././components/img/list_hover.png",
							floating : false,
							responsive : false,
							initialItemCount:1000,
							reloadItemCount:1000,
							itemMinWidth : 0,
							template : this.createTemplate()
						}) 
					],
			search : function search(oEvent) {
				var sQuery = oEvent.getParameter("query");
				var oBinding = oDataSet.getBinding("items");
				oBinding.filter(!sQuery ? [] : [ new sap.ui.model.Filter("fileName", sap.ui.model.FilterOperator.Contains,sQuery) ]);
				oDataSet.setLeadSelection(-1);
			},
			selectionChanged : function search(oEvent) {
				var busyIndicator = components.utils.Common.BusyIndicatorDialog;
				busyIndicator.open();
				var idx = oEvent.getParameter("newLeadSelectedIndex");
				var variantFile = oDataSet.getItems()[idx];
				if(variantFile != undefined) {
					sap.ui.getCore().getEventBus().publish("nav", "to", { viewName : "components.pages.details.views.variantdetails", viewId : "variantDetails", data: {
						variantPkg: pkg,
						variantFile: variantFile.getTitle()
					}});
				}
				
				busyIndicator.close();
			}
		});
		
		var download 	= new sap.ui.commons.Button({
			text : "Download",
			type : sap.m.ButtonType.Accept,
			tooltip : "Download variant file",
			press : function() {
				oController.downloadDocument(pkg);
			}
		});
		oDataSet.addToolbarItem(download);
		
		var start 	= new sap.ui.commons.Button({
			text : "Start",
			type : sap.m.ButtonType.Accept,
			tooltip : "Start translation",
			press : function() {
				oController.translator(pkg);
			}
		});
		oDataSet.addToolbarItem(start);

		var variantListPage = new sap.m.Page({
			title : "Variant List",
			customHeader : new sap.m.Bar({
				contentMiddle: 	[ new sap.m.Label({text: "Variant List"}) ],
				contentRight:	[ new sap.m.Image({src: "./././components/img/translator.jpg", decorative: true})]
			}),
			footer : new sap.m.Bar({
				contentLeft:  [ new sap.m.Button({text:"Back", type:sap.m.ButtonType.Back, press:[oController.onBack, oController]})],
				contentMiddle:[ new sap.m.Image({src: "./././components/img/viz_chart.jpg", decorative: true}) ],
				contentRight: [	new sap.m.Text({ text : "CopyRight@Packaging Framework Team" }) ]
			}),
			content : [ oDataSet ]
		});
		return variantListPage;
	},
});