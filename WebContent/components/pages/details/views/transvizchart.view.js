jQuery.sap.require("sap.viz.ui5.data.FlattenedDataset");
jQuery.sap.require("sap.viz.ui5.VizContainer");
sap.ui.jsview( "components.pages.details.views.transvizchart", {

	/**
	 * Specifies the Controller belonging to this View. In the
	 * case that it is not implemented, or that "null" is
	 * returned, this View does not have a Controller.
	 * 
	 * @memberOf components.pages.details.transvizchart
	 */
	getControllerName : function() {
		return "components.pages.details.controllers.transvizchart";
	},

	/**
	 * Is initially called once after the Controller has been
	 * instantiated. It is the place where the UI is
	 * constructed. Since the Controller is given to this
	 * method, its event handlers can be attached right away.
	 * 
	 * @memberOf components.pages.details.transvizchart
	 */
	createContent : function(oController) {
		// A Dataset defines how the model data is mapped to the chart
		var oDataset = new sap.viz.ui5.data.FlattenedDataset({
			// a Bar Chart requires exactly one dimension
			// (x-axis)
			dimensions : [ {
				axis : 1, // must be one for the x-axis, 2 for y-axis
				name : 'Country',
				value : "{Country}"
			} ],
			// it can show multiple measures, each results in a
			// new set of bars in a new color
			measures : [{// measure 1
				name : 'Profit', // 'name' is used as labelin the Legend
				value : '{profit}' // 'value' defines the binding for the displayed value
			} ],
			// 'data' is used to bind the whole data collection
			// that is to be displayed in the chart
			data : { path : "/" }
		});

		// create a VizContainer
		var oVizContainer = new sap.viz.ui5.VizContainer({
			'uiConfig' : { 'layout' : 'vertical', 'enableMorphing' : true },
			'width' : '100%',
			'height': '100%'
		});

		// attach the model to the chart and display it
		oVizContainer.setVizData(oDataset)
//		oVizContainer.setModel(oModel);

		// set feeds
		var aobjCountry = new sap.viz.ui5.controls.common.feeds.AnalysisObject({
			uid : "Country_id",
			name : "Country",
			type : "Dimension"
		}), aobjProfit = new sap.viz.ui5.controls.common.feeds.AnalysisObject({
			uid : "Profit_id",
			name : "Profit",
			type : "Measure"
		});
		var feedPrimaryValues = new sap.viz.ui5.controls.common.feeds.FeedItem({
			uid : "primaryValues",
			type : "Measure",
			values : [ aobjProfit ]
		}), feedAxisLabels = new sap.viz.ui5.controls.common.feeds.FeedItem({
			uid : "axisLabels",
			type : "Dimension",
			values : [ aobjCountry ]
		});

		oVizContainer.addFeed(feedPrimaryValues);
		oVizContainer.addFeed(feedAxisLabels);

		// attach event listener for feedschange
		oVizContainer.attachEvent('feedsChanged', function(e) {
			// You could add your own logic to handle feedsChanged to set new dataset to vizContainer. Reset current data for demo purpose.
			oVizContainer.setVizData(new sap.viz.ui5.data.FlattenedDataset({
				dimensions : [ { axis : 1, name : 'Country', value : "{Country}" } ],
				measures : [ { name : 'Profit', value : '{profit}'} ],
				data : { path : "/" }
			}));
//			oVizContainer.setModel(oModel);
			oController.assignChartData();
		});
		
		 var chartPage = new sap.m.Page({ 
			 title : "Translation Chart",
			 customHeader : new sap.m.Bar({
					contentMiddle: 	[ new sap.m.Label({text: "Variant Details"}) ],
					contentRight:	[ new sap.m.Image({src: "./././components/img/translator.jpg", decorative: true})]
				}),
			footer : new sap.m.Bar({
					contentLeft:  [ new sap.m.Button({text:"Back", type:sap.m.ButtonType.Back, press: [oController.onBack, oController] }) ],
					contentMiddle:[ new sap.m.Image({src: "./././components/img/viz_chart.jpg", decorative: true}) ],
					contentRight: [	new sap.m.Text({ text : "CopyRight@Packaging Framework Team" }) ]
				}),
			 content : [ oVizContainer ] 
		 });
		 
		oController.assignChartData();
		return chartPage;
	}
});