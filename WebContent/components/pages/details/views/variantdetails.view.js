jQuery.sap.require("sap.ui.table.Table");
sap.ui.jsview("components.pages.details.views.variantdetails", {

	/**
	 * Specifies the Controller belonging to this View. In the case that it is
	 * not implemented, or that "null" is returned, this View does not have a
	 * Controller.
	 * 
	 * @memberOf components.pages.details.variantdetails
	 */
	getControllerName : function() {
		return "components.pages.details.controllers.variantdetails";
	},
	onBeforeShow : function(evt) {  
	    this.getController().onBeforeShow(evt);  
	},
	/**
	 * Is initially called once after the Controller has been instantiated. It
	 * is the place where the UI is constructed. Since the Controller is given
	 * to this method, its event handlers can be attached right away.
	 * 
	 * @memberOf components.pages.details.variantdetails
	 */
	createContent : function(oController) {
		var variantPkg;
		// Global variables
		this.setHeight("100%");
		this.addEventDelegate({  
			onBeforeShow: function(evt) {  
				var data = evt.data;
				variantPkg  = data.variantPkg;
				oController.fetchVariantFileData(data);
			}
		});
		
		oController.extendItemlayout();
		var oTable = new sap.ui.table.Table("details", {
			title : "Details", visibleRowCount : 10, firstVisibleRow : 0, selectionMode : sap.ui.table.SelectionMode.Single,toolbar : this.buildToolbar()//oToolbar
		});
		
		var page = new sap.m.Page({
			title : "Variant Details",
			customHeader : new sap.m.Bar({
				contentMiddle: 	[ new sap.m.Label({text: "Variant Details"}) ],
				contentRight:	[ new sap.m.Image({src: "./././components/img/translator.jpg", decorative: true})]
			}),
			footer : new sap.m.Bar({
				contentLeft:  [ new sap.m.Button({text:"Back", type:sap.m.ButtonType.Back, press:function() {
					 oController.onBack(variantPkg);
				}}) ],
				contentMiddle:[ new sap.m.Image({src: "./././components/img/viz_chart.jpg", decorative: true}) ],
				contentRight: [	new sap.m.Text({ text : "CopyRight@Packaging Framework Team" }) ]
			}),
			content : [ oTable ]
		});
		return page;
	},
	
	buildToolbar: function() {
		var that = this.getController();
		// Creating the toolbar control
		var oToolbar 	= new sap.ui.commons.Toolbar();
		var start 		= new sap.ui.commons.Button("submit", {
			text : "Submit",
			type : sap.m.ButtonType.Accept,
			tooltip : "Save changing data",
			press : function() {
				that.updateVariantFile();
			}
		});
		start.setEnabled(false);
		oToolbar.addRightItem(start);
		return oToolbar;
	}
});