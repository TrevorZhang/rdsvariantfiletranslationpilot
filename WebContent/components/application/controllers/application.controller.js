jQuery.sap.require("jquery.sap.history");
jQuery.sap.require("sap.m.InstanceManager");
jQuery.sap.require("sap.ui.app.Application");
sap.ui.controller("components.application.controllers.application", {

/**
* Called when a controller is instantiated and its View controls (if available) are already created.
* Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
* @memberOf components.transapp
*/
	onInit : function () {
		var historyDefaultHandler = function (navType) {
			if (navType === jQuery.sap.history.NavType.Back) {
				var app = this.getView().app
				this.navBack(app.getInitialPage());
			} else {
				this.navTo(this.getPreviousPage(), null, false);
			}
		};
		
		var historyPageHandler = function (params, navType) {
			if (!params || !params.id) {
				jQuery.sap.log.error("invalid parameter: " + params);
			} else {
				if (navType === jQuery.sap.history.NavType.Back) {
					this.navBack(params.id);
				} else {
					this.navTo(params.id, params.data, false);
				}
			}
		};
		
		jQuery.sap.history({
			routes: [{
				// This handler is executed when you navigate back to the history state on the path "page"
				path : "page",
				handler : jQuery.proxy(historyPageHandler, this)
			}],
			// The default handler is executed when you navigate back to the history state with an empty hash
			defaultHandler: jQuery.proxy(historyDefaultHandler, this)
		});
		
		// subscribe to event bus
		var bus = sap.ui.getCore().getEventBus();
		bus.subscribe("nav", "to", this.navHandler, this);
		bus.subscribe("nav", "back", this.navHandler, this);
		bus.subscribe("nav", "virtual", this.navHandler, this);
	},
	
	navHandler: function (channelId, eventId, data) {
		if (eventId === "to") {
			this.navTo(data, true);
		} else if (eventId === "back") {
			jQuery.sap.history.back();
		} else if (eventId === "virtual") {
			jQuery.sap.history.addVirtualHistory();
		} else {
			jQuery.sap.log.error("'nav' event cannot be processed. There's no handler registered for event with id: " + eventId);
		}
	},
	
	navTo : function (oData, writeHistory) {
		var id 		= oData.viewId;
		var data 	= oData.data;
		var view 	= oData.viewName;
		
		if (id == undefined) {
			if (oData != "Master") {
				// invalid parameter
				jQuery.sap.log.error("navTo failed due to missing id");
			}
		} else {
			var app = this.getView().app;
			if (app.getPage(id) == null) {
				var page = sap.ui.jsview(id, view);
				app.addPage(page);
			}
			jQuery.sap.log.info("app controller > loaded page: " + id);
			// navigate in the app control
			app.to(id, "slide", data);
			// write browser history
			if ((writeHistory == undefined || writeHistory) &&
				(jQuery.device.is.phone )) {
				jQuery.sap.history.addHistory("page", { id: id }, false);
			}
			// log
			jQuery.sap.log.info("navTo - to page: " + id + " [" + writeHistory + "]");
		}
	},
	
	navBack : function (id) {
		if (!id) {
			// invalid parameter
			jQuery.sap.log.error("navBack - parameters id must be given");
		} else {
			// close open popovers
			if (sap.m.InstanceManager.hasOpenPopover()) {
				sap.m.InstanceManager.closeAllPopovers();
			}
			
			// close open dialogs
			if (sap.m.InstanceManager.hasOpenDialog()) {
				sap.m.InstanceManager.closeAllDialogs();
				jQuery.sap.log.info("navBack - closed dialog(s)");
			}
			
			// ... and navigate back
			var app = this.getView().app;
			var currentId = (app.getCurrentPage()) ? app.getCurrentPage().getId() : null;
			if (currentId !== id) {
				app.backToPage(id);
				jQuery.sap.log.info("navBack - back to page: " + id);
			}
		}
	}

/**
* Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
* (NOT before the first rendering! onInit() is used for that one!).
* @memberOf components.transapp
*/
//	onBeforeRendering: function() {
//
//	},

/**
* Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
* This hook is the same one that SAPUI5 controls get after being rendered.
* @memberOf components.transapp
*/
//	onAfterRendering: function() {
//
//	},

/**
* Called when the Controller is destroyed. Use this one to free resources and finalize activities.
* @memberOf components.transapp
*/
//	onExit: function() {
//
//	}

});