sap.ui.jsview("components.application.views.application", {
	/** Specifies the Controller belonging to this View. 
	* In the case that it is not implemented, or that "null" is returned, this View does not have a Controller.
	* @memberOf components.transapp
	*/ 
	getControllerName : function() {
		return "components.application.controllers.application";
	},

	/** Is initially called once after the Controller has been instantiated. It is the place where the UI is constructed. 
	* Since the Controller is given to this method, its event handlers can be attached right away. 
	* @memberOf components.transapp
	*/ 
	createContent : function(oController) {
		this.app = new sap.m.App("application", { initialPage : "navPage" }); // page1 should be displayed first
		var mavigatorPage = sap.ui.view({id:"navPage", viewName:"components.pages.navigator.views.navigator", type:sap.ui.core.mvc.ViewType.JS});
		this.app.addPage(mavigatorPage);

	    return this.app;
	}
});