jQuery.sap.declare("components.utils.Common");
components.utils.Common = function() {
};

components.utils.Common.FooterBar = new sap.m.Bar({
	enableFlexBox : true,
	contentLeft :	new sap.m.Text("sandBox", 	{ text : "sandBox" }),
	contentMiddle: 	new sap.m.Button("accept", 	{ type : sap.m.ButtonType.Accept ,icon:"./././components/img/vizcontainer.png", press: function() {
//		var busyIndicator = components.utils.Common.BusyIndicatorDialog;
//		busyIndicator.open();
//		sap.ui.getCore().getEventBus().publish("nav", "to", { viewName : "components.pages.details.views.transvizchart", viewId : "viz_chart" });
//		busyIndicator.close();
	}}),
	contentRight : 	new sap.m.Text("CopyRight", { text : "CopyRight@Packaging Framework Team" }),
});

components.utils.Common.BusyIndicatorDialog = new sap.m.BusyDialog({customIcon: "./././components/img/synchronise_48.png"});
