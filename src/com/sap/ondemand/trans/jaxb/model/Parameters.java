/**
 * 
 */
package com.sap.ondemand.trans.jaxb.model;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;

/**
 * @author I075006
 *
 */
public class Parameters {

	private List<ParametersItem> item;

	/**
	 * @return the item
	 */
	@XmlElement(name = "item")
	public List<ParametersItem> getItem() {
		return item;
	}

	/**
	 * @param item the item to set
	 */
	public void setItem(List<ParametersItem> item) {
		this.item = item;
	}
}
