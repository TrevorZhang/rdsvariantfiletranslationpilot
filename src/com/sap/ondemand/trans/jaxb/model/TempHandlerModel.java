/**
 * 
 */
package com.sap.ondemand.trans.jaxb.model;

/**
 * @author I075006
 *
 */
public class TempHandlerModel {
	
	private String folderName;
	private boolean isDocument;
	
	public TempHandlerModel() {
		
	}
	
	public TempHandlerModel(String folderName, boolean isDocument) {
		this.folderName = folderName;
		this.isDocument = isDocument;
	}
	
	/**
	 * @return the folderName
	 */
	public String getFolderName() {
		return folderName;
	}
	/**
	 * @param folderName the folderName to set
	 */
	public void setFolderName(String folderName) {
		this.folderName = folderName;
	}
	/**
	 * @return the isDocument
	 */
	public boolean isDocument() {
		return isDocument;
	}
	/**
	 * @param isDocument the isDocument to set
	 */
	public void setDocument(boolean isDocument) {
		this.isDocument = isDocument;
	}
	
	

}
