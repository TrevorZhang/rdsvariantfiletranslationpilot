/**
 * 
 */
package com.sap.ondemand.trans.jaxb.model;

import javax.xml.bind.annotation.XmlElement;

/**
 * @author I075006
 *
 */
public class TargetLanguagesItem {

	private String laiso;
	private String usedByEcatt;
	
	/**
	 * @return the laiso
	 */
	@XmlElement(name = "LAISO")
	public String getLaiso() {
		return laiso;
	}
	/**
	 * @param laiso the laiso to set
	 */
	public void setLaiso(String laiso) {
		this.laiso = laiso;
	}
	/**
	 * @return the usedByEcatt
	 */
	@XmlElement(name = "USED_BY_ECATT")
	public String getUsedByEcatt() {
		return usedByEcatt;
	}
	/**
	 * @param usedByEcatt the usedByEcatt to set
	 */
	public void setUsedByEcatt(String usedByEcatt) {
		this.usedByEcatt = usedByEcatt;
	}
}
