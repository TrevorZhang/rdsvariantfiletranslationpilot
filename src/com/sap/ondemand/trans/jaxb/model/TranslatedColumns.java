/**
 * 
 */
package com.sap.ondemand.trans.jaxb.model;

import java.util.List;

/**
 * @author I075006
 *
 */
public class TranslatedColumns {
	
	private String 			preferenceColumn;
	private List<String> 	targetColumns;
	
	/**
	 * @return the preferenceColumn
	 */
	public String getPreferenceColumn() {
		return preferenceColumn;
	}
	/**
	 * @param preferenceColumn the preferenceColumn to set
	 */
	public void setPreferenceColumn(String preferenceColumn) {
		this.preferenceColumn = preferenceColumn;
	}
	/**
	 * @return the targetColumns
	 */
	public List<String> getTargetColumns() {
		return targetColumns;
	}
	/**
	 * @param targetColumns the targetColumns to set
	 */
	public void setTargetColumns(List<String> targetColumns) {
		this.targetColumns = targetColumns;
	}
}
