/**
 * 
 */
package com.sap.ondemand.trans.jaxb.model;

/**
 * @author I075006
 *
 */
public class MappingModel {
	
	private ParametersItem preferenceItem;
	private ParametersItem targetItem;
	
	public MappingModel() {
	}
	public MappingModel(ParametersItem preferenceItem, ParametersItem targetItem) {
		this.preferenceItem = preferenceItem;
		this.targetItem = targetItem;
				
	}
	
	/**
	 * @return the preferenceItem
	 */
	public ParametersItem getPreferenceItem() {
		return preferenceItem;
	}
	/**
	 * @param preferenceItem the preferenceItem to set
	 */
	public void setPreferenceItem(ParametersItem preferenceItem) {
		this.preferenceItem = preferenceItem;
	}
	/**
	 * @return the targetItem
	 */
	public ParametersItem getTargetItem() {
		return targetItem;
	}
	/**
	 * @param targetItem the targetItem to set
	 */
	public void setTargetItem(ParametersItem targetItem) {
		this.targetItem = targetItem;
	}
}
