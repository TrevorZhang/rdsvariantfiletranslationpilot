/**
 * 
 */
package com.sap.ondemand.trans.jaxb.model;

import java.util.List;

/**
 * @author I075006
 *
 */
public class VariantMappingModel {
	
	private List<String> referenceColumns;
	private List<String> targetColumns;
	
	/**
	 * @return the referenceColumns
	 */
	public List<String> getReferenceColumns() {
		return referenceColumns;
	}
	/**
	 * @param referenceColumns the referenceColumns to set
	 */
	public void setReferenceColumns(List<String> referenceColumns) {
		this.referenceColumns = referenceColumns;
	}
	/**
	 * @return the targetColumns
	 */
	public List<String> getTargetColumns() {
		return targetColumns;
	}
	/**
	 * @param targetColumns the targetColumns to set
	 */
	public void setTargetColumns(List<String> targetColumns) {
		this.targetColumns = targetColumns;
	}
}
