/**
 * 
 */
package com.sap.ondemand.trans.jaxb.model;

/**
 * @author I075006
 *
 */
public class TranslationColumnsInfo {

	private int referenceColumnIndex;
	private int translatedColumnIndex;
	
	public TranslationColumnsInfo() {
	}
	public TranslationColumnsInfo(int referenceColumnIndex, int translatedColumnIndex) {
		this.referenceColumnIndex = referenceColumnIndex;
		this.translatedColumnIndex = translatedColumnIndex;
	}
	
	/**
	 * @return the referenceColumnIndex
	 */
	public int getReferenceColumnIndex() {
		return referenceColumnIndex;
	}
	/**
	 * @param referenceColumnIndex the referenceColumnIndex to set
	 */
	public void setReferenceColumnIndex(int referenceColumnIndex) {
		this.referenceColumnIndex = referenceColumnIndex;
	}
	
	/**
	 * @return the translatedColumnIndex
	 */
	public int getTranslatedColumnIndex() {
		return translatedColumnIndex;
	}
	/**
	 * @param translatedColumnIndex the translatedColumnIndex to set
	 */
	public void setTranslatedColumnIndex(int translatedColumnIndex) {
		this.translatedColumnIndex = translatedColumnIndex;
	}
	
}
