/**
 * 
 */
package com.sap.ondemand.trans.jaxb.model;

/**
 * @author I075006
 *
 */
public class LocationOffset {

	private int index;
	private int sourceIdx;
	private int targetIdx;
	private boolean isExist;
	private String preferenceLang;
	private String targetLang;
	
	public LocationOffset() {
		
	}
	public LocationOffset(int index, int sourceIdx, int targetIdx, boolean isExist, String preferenceLang, String targetLang) {
		this.index 		= index;
		this.sourceIdx 	= sourceIdx;
		this.targetIdx 	= targetIdx;
		this.isExist 	= isExist;
		this.preferenceLang = preferenceLang;
		this.targetLang = targetLang;
	}
	/**
	 * @return the index
	 */
	public int getIndex() {
		return index;
	}
	/**
	 * @param index the index to set
	 */
	public void setIndex(int index) {
		this.index = index;
	}
	/**
	 * @return the sourceIdx
	 */
	public int getSourceIdx() {
		return sourceIdx;
	}
	/**
	 * @param sourceIdx the sourceIdx to set
	 */
	public void setSourceIdx(int sourceIdx) {
		this.sourceIdx = sourceIdx;
	}
	/**
	 * @return the targetIdx
	 */
	public int getTargetIdx() {
		return targetIdx;
	}
	/**
	 * @param targetIdx the targetIdx to set
	 */
	public void setTargetIdx(int targetIdx) {
		this.targetIdx = targetIdx;
	}
	/**
	 * @return the isExist
	 */
	public boolean isExist() {
		return isExist;
	}
	/**
	 * @param isExist the isExist to set
	 */
	public void setExist(boolean isExist) {
		this.isExist = isExist;
	}
	/**
	 * @return the preferenceLang
	 */
	public String getPreferenceLang() {
		return preferenceLang;
	}
	/**
	 * @param preferenceLang the preferenceLang to set
	 */
	public void setPreferenceLang(String preferenceLang) {
		this.preferenceLang = preferenceLang;
	}
	/**
	 * @return the targetLang
	 */
	public String getTargetLang() {
		return targetLang;
	}
	/**
	 * @param targetLang the targetLang to set
	 */
	public void setTargetLang(String targetLang) {
		this.targetLang = targetLang;
	}
}
