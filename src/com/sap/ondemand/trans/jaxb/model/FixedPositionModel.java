/**
 * 
 */
package com.sap.ondemand.trans.jaxb.model;

/**
 * @author I075006
 *
 */
public class FixedPositionModel {
	
	private int sourceLoc;
	private int targetLoc;
	private boolean isTranslated;
	private int preferenceLoc;
	
	private String ecatRefereneceLang;
	private String ecatTargetLang;
	
	public FixedPositionModel() {
		
	}
	public FixedPositionModel(int sourceLoc, int targetLoc, boolean isTranslated, int preferenceLoc, String ecatRefereneceLang, String ecatTargetLang) {
		this.sourceLoc = sourceLoc;
		this.targetLoc = targetLoc;
		this. isTranslated = isTranslated;
		this.preferenceLoc = preferenceLoc;
		this.ecatRefereneceLang = ecatRefereneceLang;
		this.ecatTargetLang = ecatTargetLang;
	}
	
	/**
	 * @return the sourceLoc
	 */
	public int getSourceLoc() {
		return sourceLoc;
	}
	/**
	 * @param sourceLoc the sourceLoc to set
	 */
	public void setSourceLoc(int sourceLoc) {
		this.sourceLoc = sourceLoc;
	}
	/**
	 * @return the targetLoc
	 */
	public int getTargetLoc() {
		return targetLoc;
	}
	/**
	 * @param targetLoc the targetLoc to set
	 */
	public void setTargetLoc(int targetLoc) {
		this.targetLoc = targetLoc;
	}
	/**
	 * @return the isTranslated
	 */
	public boolean isTranslated() {
		return isTranslated;
	}
	/**
	 * @param isTranslated the isTranslated to set
	 */
	public void setTranslated(boolean isTranslated) {
		this.isTranslated = isTranslated;
	}
	/**
	 * @return the preferenceLoc
	 */
	public int getPreferenceLoc() {
		return preferenceLoc;
	}
	/**
	 * @param preferenceLoc the preferenceLoc to set
	 */
	public void setPreferenceLoc(int preferenceLoc) {
		this.preferenceLoc = preferenceLoc;
	}
	/**
	 * @return the ecatRefereneceLang
	 */
	public String getEcatRefereneceLang() {
		return ecatRefereneceLang;
	}
	/**
	 * @param ecatRefereneceLang the ecatRefereneceLang to set
	 */
	public void setEcatRefereneceLang(String ecatRefereneceLang) {
		this.ecatRefereneceLang = ecatRefereneceLang;
	}
	/**
	 * @return the ecatTargetLang
	 */
	public String getEcatTargetLang() {
		return ecatTargetLang;
	}
	/**
	 * @param ecatTargetLang the ecatTargetLang to set
	 */
	public void setEcatTargetLang(String ecatTargetLang) {
		this.ecatTargetLang = ecatTargetLang;
	}
}
