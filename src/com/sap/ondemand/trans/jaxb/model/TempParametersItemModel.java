/**
 * 
 */
package com.sap.ondemand.trans.jaxb.model;

/**
 * @author I075006
 *
 */
public class TempParametersItemModel {

	private String column;

	/**
	 * @return the column
	 */
	public String getColumn() {
		return column;
	}

	/**
	 * @param column the column to set
	 */
	public void setColumn(String column) {
		this.column = column;
	}
}
