/**
 * 
 */
package com.sap.ondemand.trans.jaxb.model;

/**
 * @author I075006
 *
 */
public class ZipObjectModel {
	
	private String objectId;
	private String fileName;
	
	public ZipObjectModel() {
		
	}
	public ZipObjectModel(String objectId, String fileName) {
		this.objectId = objectId;
		this.fileName = fileName;
	}
	
	/**
	 * @return the objectId
	 */
	public String getObjectId() {
		return objectId;
	}
	/**
	 * @param objectId the objectId to set
	 */
	public void setObjectId(String objectId) {
		this.objectId = objectId;
	}
	/**
	 * @return the fileName
	 */
	public String getFileName() {
		return fileName;
	}
	/**
	 * @param fileName the fileName to set
	 */
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

}
