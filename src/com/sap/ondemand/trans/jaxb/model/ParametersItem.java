/**
 * 
 */
package com.sap.ondemand.trans.jaxb.model;

import javax.xml.bind.annotation.XmlElement;

/**
 * @author I075006
 *
 */
public class ParametersItem {
	
	private String paramLang;
	private String paramName;
	private String paramDataElement;
	private String abapType;
	private String length;
	private String seqnum;
	private String ecatPara;
	private String oriLang;
	
	/**
	 * @return the paramLang
	 */
	@XmlElement(name = "PARAM_LANG")
	public String getParamLang() {
		return paramLang;
	}
	/**
	 * @param paramLang the paramLang to set
	 */
	public void setParamLang(String paramLang) {
		this.paramLang = paramLang;
	}
	/**
	 * @return the paramName
	 */
	@XmlElement(name = "PARA_NAME")
	public String getParamName() {
		return paramName;
	}
	/**
	 * @param paramName the paramName to set
	 */
	public void setParamName(String paramName) {
		this.paramName = paramName;
	}
	/**
	 * @return the paramDataElement
	 */
	@XmlElement(name = "PARA_DATAELEMENT")
	public String getParamDataElement() {
		return paramDataElement;
	}
	/**
	 * @param paramDataElement the paramDataElement to set
	 */
	public void setParamDataElement(String paramDataElement) {
		this.paramDataElement = paramDataElement;
	}
	/**
	 * @return the abapType
	 */
	@XmlElement(name = "ABAP_TYPE")
	public String getAbapType() {
		return abapType;
	}
	/**
	 * @param abapType the abapType to set
	 */
	public void setAbapType(String abapType) {
		this.abapType = abapType;
	}
	/**
	 * @return the length
	 */
	@XmlElement(name = "LENGTH")
	public String getLength() {
		return length;
	}
	/**
	 * @param length the length to set
	 */
	public void setLength(String length) {
		this.length = length;
	}
	/**
	 * @return the seqnum
	 */
	@XmlElement(name = "SEQNUM")
	public String getSeqnum() {
		return seqnum;
	}
	/**
	 * @param seqnum the seqnum to set
	 */
	public void setSeqnum(String seqnum) {
		this.seqnum = seqnum;
	}
	/**
	 * @return the seqnum
	 */
	@XmlElement(name = "ECAT_PARAM")
	public String getEcatPara() {
		return ecatPara;
	}
	/**
	 * @param seqnum the seqnum to set
	 */
	public void setEcatPara(String ecatPara) {
		this.ecatPara = ecatPara;
	}
	/**
	 * @return the seqnum
	 */
	@XmlElement(name = "ORI_LANG")
	public String getOriLang() {
		return oriLang;
	}
	/**
	 * @param seqnum the seqnum to set
	 */
	public void setOriLang(String oriLang) {
		this.oriLang = oriLang;
	}
}
