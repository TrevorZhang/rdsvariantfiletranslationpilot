/**
 * 
 */
package com.sap.ondemand.trans.jaxb.model;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author I075006
 *
 */
public class TranslatedMappingHelper {
	
	public static Map<String, Map<String, MappingModel>> getMapping(List<TasksItem> taskItems, String preferenceLang, String targetLang) {
		Map<String, Map<String, MappingModel>> mapping = new HashMap<String, Map<String, MappingModel>>();
		Map<String, MappingModel> subMapping = null;
		for (TasksItem tasksItem : taskItems) {
			List<ParametersItem> paramItems = tasksItem.getParameters().getItem();
			subMapping = new HashMap<String, MappingModel>();
			if(tasksItem.getObjty().equals("ECAT")) {
				for (ParametersItem parametersItem : paramItems) {
					if(parametersItem.getEcatPara() != null) {
						subMapping.put(parametersItem.getParamName(), new MappingModel(getEctReferenceItem(paramItems, parametersItem.getEcatPara()), parametersItem));
					}
				}
			} else {
				for (ParametersItem parametersItem : paramItems) {
					if(parametersItem.getParamLang().equals(targetLang)) {
						ParametersItem item = getReferenceItem(paramItems, parametersItem.getParamName(), preferenceLang);
						subMapping.put("I_" + item.getParamName() + "_" + item.getParamLang(), new MappingModel(item, parametersItem));
					}
				}
				mapping.put(tasksItem.getFileName(), subMapping);
			}
			mapping.put(tasksItem.getFileName(), subMapping);
		}
		
		return mapping;
	}
	
	private static ParametersItem getReferenceItem(List<ParametersItem> paraItems, String paramName, String preferenceLang) {
		ParametersItem param = null;
		for (ParametersItem parametersItem : paraItems) {
			if(parametersItem.getParamLang().equals(preferenceLang) && parametersItem.getParamName().equals(paramName)) {
				param = parametersItem;
			}
		}
		return param;
	}
	
	private static ParametersItem getEctReferenceItem(List<ParametersItem> paraItems, String paraName) {
		ParametersItem param = null;
		for (ParametersItem parametersItem : paraItems) {
			if(parametersItem.getParamName().equals(paraName)) {
				param = parametersItem;
			}
		}
		
		return param;
	}
}
