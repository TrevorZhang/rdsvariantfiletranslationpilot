/**
 * 
 */
package com.sap.ondemand.trans.jaxb.model;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;

/**
 * @author I075006
 *
 */
public class Tasks {

	private List<TasksItem> item;
	
	/**
	 * @return the item
	 */
	@XmlElement(name = "item")
	public List<TasksItem> getItem() {
		return item;
	}
	/**
	 * @param item the item to set
	 */
	public void setItem(List<TasksItem> item) {
		this.item = item;
	}
}
