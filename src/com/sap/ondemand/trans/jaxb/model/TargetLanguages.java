/**
 * 
 */
package com.sap.ondemand.trans.jaxb.model;

import javax.xml.bind.annotation.XmlElement;

/**
 * @author I075006
 *
 */
public class TargetLanguages {

	private TargetLanguagesItem item;

	/**
	 * @return the item
	 */
	@XmlElement(name = "item")
	public TargetLanguagesItem getItem() {
		return item;
	}

	/**
	 * @param item the item to set
	 */
	public void setItem(TargetLanguagesItem item) {
		this.item = item;
	}
}
