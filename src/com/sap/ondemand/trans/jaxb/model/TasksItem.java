/**
 * 
 */
package com.sap.ondemand.trans.jaxb.model;

import javax.xml.bind.annotation.XmlElement;

/**
 * @author I075006
 *
 */
public class TasksItem {

	private String objty;
	private String objid;
	private String fileName;
	private String createDate;
	private String createTime;
	private String user;
	private Parameters parameters;
	
	/**
	 * @return the objty
	 */
	@XmlElement(name = "OBJTY")
	public String getObjty() {
		return objty;
	}
	/**
	 * @param objty the objty to set
	 */
	public void setObjty(String objty) {
		this.objty = objty;
	}
	/**
	 * @return the objid
	 */
	@XmlElement(name = "OBJID")
	public String getObjid() {
		return objid;
	}
	/**
	 * @param objid the objid to set
	 */
	public void setObjid(String objid) {
		this.objid = objid;
	}
	/**
	 * @return the fileName
	 */
	@XmlElement(name = "FILENAME")
	public String getFileName() {
		return fileName;
	}
	/**
	 * @param fileName the fileName to set
	 */
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	/**
	 * @return the createDate
	 */
	@XmlElement(name = "CREATE_DATE")
	public String getCreateDate() {
		return createDate;
	}
	/**
	 * @param createDate the createDate to set
	 */
	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}
	/**
	 * @return the createTime
	 */
	@XmlElement(name = "CREATE_TIME")
	public String getCreateTime() {
		return createTime;
	}
	/**
	 * @param createTime the createTime to set
	 */
	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}
	/**
	 * @return the user
	 */
	@XmlElement(name = "USER")
	public String getUser() {
		return user;
	}
	/**
	 * @param user the user to set
	 */
	public void setUser(String user) {
		this.user = user;
	}
	/**
	 * @return the parameters
	 */
	@XmlElement(name = "PARAMETERS")
	public Parameters getParameters() {
		return parameters;
	}
	/**
	 * @param parameters the parameters to set
	 */
	public void setParameters(Parameters parameters) {
		this.parameters = parameters;
	}
}
