/**
 * 
 */
package com.sap.ondemand.trans.jaxb.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author I075006
 *
 */
@XmlRootElement(name="TranslationTasks")
public class TranslationTasksModel {
	
	private String solutionId;
	private String referenceLangu;
	private String replaceLangu;
	private TargetLanguages targetLangus;
	private Tasks tasks;
	
	/**
	 * @return the solutionId
	 */
	@XmlElement(name= "SOLUTION_ID")
	public String getSolutionId() {
		return solutionId;
	}
	/**
	 * @param solutionId the solutionId to set
	 */
	public void setSolutionId(String solutionId) {
		this.solutionId = solutionId;
	}
	/**
	 * @return the referenceLangu
	 */
	@XmlElement(name= "REFERENCE_LANGU")
	public String getReferenceLangu() {
		return referenceLangu;
	}
	/**
	 * @param referenceLangu the referenceLangu to set
	 */
	public void setReferenceLangu(String referenceLangu) {
		this.referenceLangu = referenceLangu;
	}
	/**
	 * @return the replaceLangu
	 */
	@XmlElement(name = "REPLACE_LANGU")
	public String getReplaceLangu() {
		return replaceLangu;
	}
	/**
	 * @param replaceLangu the replaceLangu to set
	 */
	public void setReplaceLangu(String replaceLangu) {
		this.replaceLangu = replaceLangu;
	}
	/**
	 * @return the targetLangus
	 */
	@XmlElement(name = "TARGET_LANGUS")
	public TargetLanguages getTargetLangus() {
		return targetLangus;
	}
	/**
	 * @param targetLangus the targetLangus to set
	 */
	public void setTargetLangus(TargetLanguages targetLangus) {
		this.targetLangus = targetLangus;
	}
	/**
	 * @return the tasks
	 */
	@XmlElement(name = "TASKS")
	public Tasks getTasks() {
		return tasks;
	}
	/**
	 * @param tasks the tasks to set
	 */
	public void setTasks(Tasks tasks) {
		this.tasks = tasks;
	}
}
