/**
 * 
 */
package com.sap.ondemand.trans.jaxb;

import java.io.File;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import com.sap.ondemand.trans.jaxb.model.TranslationTasksModel;

/**
 * @author I075006
 *
 */
public class JAXBHelper {
	
	public static Object onHeloper(File xmlFile, Class<TranslationTasksModel> obj) throws JAXBException, InstantiationException, IllegalAccessException {
		JAXBContext jaxbContext = JAXBContext.newInstance(obj);

		Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
		Object objInstance = obj.newInstance();
		objInstance = jaxbUnmarshaller.unmarshal(xmlFile);
		return objInstance;

	}
//	
//	public static void main(String[] args) {
//		try {
//			JAXBHelper.onHeloper(TranslationTasksModel.class);
//		} catch (JAXBException e) {
//			e.printStackTrace();
//		} catch (InstantiationException e) {
//			e.printStackTrace();
//		} catch (IllegalAccessException e) {
//			e.printStackTrace();
//		}
//	}
}
