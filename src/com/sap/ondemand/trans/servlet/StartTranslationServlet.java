/**
 * 
 */
package com.sap.ondemand.trans.servlet;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipException;
import java.util.zip.ZipFile;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.apache.chemistry.opencmis.client.api.CmisObject;
import org.apache.chemistry.opencmis.client.api.Document;
import org.apache.chemistry.opencmis.client.api.Folder;
import org.apache.chemistry.opencmis.client.api.ItemIterable;
import org.apache.chemistry.opencmis.commons.data.ContentStream;
import org.apache.commons.lang.StringEscapeUtils;

import com.sap.ondemand.trans.helper.CmisHelper;
import com.sap.ondemand.trans.helper.GoogleTranslator;
import com.sap.ondemand.trans.helper.GsonHelper;
import com.sap.ondemand.trans.helper.UTF2GBK;
import com.sap.ondemand.trans.jaxb.model.FileContentSensorModel;
import com.sap.ondemand.trans.jaxb.model.FixedPositionModel;
import com.sap.ondemand.trans.jaxb.model.MappingModel;
import com.sap.ondemand.trans.jaxb.model.ParametersItem;
import com.sap.ondemand.trans.jaxb.model.Tasks;
import com.sap.ondemand.trans.jaxb.model.TasksItem;
import com.sap.ondemand.trans.jaxb.model.TranslatedMappingHelper;
import com.sap.ondemand.trans.jaxb.model.TranslationTasksModel;
import com.sap.ondemand.trans.jaxb.model.ZipObjectModel;
import com.sap.ondemand.trans.messages.MessageHandler;

/**
 * @author I075006
 *
 */
public class StartTranslationServlet extends HttpServlet {

	private static final long serialVersionUID = -3592031872489116425L;
	private CmisHelper cmisHelper = new CmisHelper();
	public static final String TAB = "	";
	
	
	/* (non-Javadoc)
	 * @see javax.servlet.http.HttpServlet#doPost(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse response)throws ServletException, IOException {
		response.setContentType("text/html;charset=utf-8");
        response.setHeader("Cache-Control", "no-cache"); 
        PrintWriter out = response.getWriter(); 
        MessageHandler messageHandler = new MessageHandler();
		String pkg 		= req.getParameter("pkg");
		try {
			doTranslation(pkg);
			messageHandler.setStatus("Success");
			messageHandler.setMessage("Success");
		} catch (Exception ex) {
			messageHandler.setMessage(ex.getMessage());
		}
		out.print(GsonHelper.toJson(messageHandler));
		
		out.flush();
		out.close();
	}

	/**
	 * 
	 * @param pkgName
	 * @return
	 * @throws Exception 
	 */
	private File getVariantPkgFile(String pkgName) throws Exception {
		ZipObjectModel fileModel = cmisHelper.getDocumentProperties(pkgName);
		Document document = cmisHelper.getDocumentById(fileModel.getObjectId());
		InputStream inputStream = document.getContentStream().getStream();
		File zipCmisObject = new File(document.getName());
		OutputStream os = new FileOutputStream(zipCmisObject);
		byte[] buffer = new byte[1024];
		int bytesRead;
		// read from is to buffer
		while ((bytesRead = inputStream.read(buffer)) != -1) {
			os.write(buffer, 0, bytesRead);
		}
		inputStream.close();
		os.flush();
		os.close();
		
		return zipCmisObject;
	}
	
	/**
	 * 
	 * @param pkg
	 * @return
	 * @throws ZipException
	 * @throws IOException
	 */
	private Map<String, FileContentSensorModel> getVariantMap(ZipFile pkg) throws ZipException, IOException {
		ZipEntry entry 	= null;
		Map<String, FileContentSensorModel> fileMap = new HashMap<String, FileContentSensorModel>();
		Enumeration<? extends ZipEntry> enumeration = pkg.entries();
		String fileName = "";
		while (enumeration.hasMoreElements()) {
			entry = (ZipEntry) enumeration.nextElement();
			if(!entry.isDirectory()) {
				if(entry.getName().contains("/")) {
					fileName = entry.getName().substring(entry.getName().lastIndexOf("/")+1, entry.getName().length() );;
				} else {
					fileName = entry.getName();
				}
				
				FileContentSensorModel sensorModel = getContentSensor(pkg.getInputStream(entry));
				sensorModel.setInputStream(pkg.getInputStream(entry));
				fileMap.put(fileName, sensorModel);
			}
		}
		
		return fileMap;
	}
	
	private FileContentSensorModel getContentSensor(InputStream inputStream) throws IOException {
		FileContentSensorModel sensorModel = new FileContentSensorModel();
		int rows 	= 0;
		int columns = 0;
		String line;
		BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "unicode"));
		while ((line = bufferedReader.readLine()) != null) {
			if(line.length() > 0) {
				if(rows == 0) {
					columns = line.split("	").length;
				}
				rows ++;
			}
		}
		sensorModel.setRows(rows);
		sensorModel.setColumns(columns);
		
		return sensorModel;
	}

	private void doTranslation(String documentId) throws Exception {
		File variantPkg = getVariantPkgFile(documentId + ".zip");
		ZipFile zipfile = new ZipFile(variantPkg);
		Map<String, FileContentSensorModel> variantMap = getVariantMap(zipfile); 
		
		String line = "";
		int rowNum  = 0;
		Folder documentFolder 						= cmisHelper.getFolderOfApp(documentId);
		BufferedReader bufferedReader				= null;
		ItemIterable<CmisObject> cmisIterator 		= documentFolder.getChildren();
		TranslationTasksModel taskModel 			= getMatchTasksModel(cmisIterator);
		Tasks tasks 								= taskModel.getTasks();
		List<TasksItem> taskItems 					= tasks.getItem();
		Map<Integer, FixedPositionModel> newDataMap	= null;
		String positionModel[][] = null;
		
		Map<String, Map<String, MappingModel>> fileMapping = TranslatedMappingHelper.getMapping(taskItems, taskModel.getReferenceLangu(), taskModel.getTargetLangus().getItem().getLaiso());
		FileContentSensorModel sensorModel = null;
		Map<String, MappingModel> subMapping = null;
		for (TasksItem tasksItem : taskItems) {
			newDataMap	= new HashMap<Integer, FixedPositionModel>();
			
			subMapping = fileMapping.get(tasksItem.getFileName());
			sensorModel 							= variantMap.get(tasksItem.getFileName());
			bufferedReader 							= new BufferedReader(new InputStreamReader(sensorModel.getInputStream(), "unicode"));
			
			if(tasksItem.getObjty().equals("ECAT")) {// Check object type
				rowNum									= 0;
				int standardColumnsNumber				= 0;
				while ((line = bufferedReader.readLine()) != null) {
					if(!"".equals(line)) {
						line = stringEncoding(line);
						String[] columnStr = line.split(TAB);
						if(rowNum == 0) {
							standardColumnsNumber = columnStr.length;
							positionModel = new String[sensorModel.getRows()][standardColumnsNumber];	// new data map
							for (int idx = 0; idx < standardColumnsNumber; idx++) {
								MappingModel mappingModel = subMapping.get(columnStr[idx]);
								if(mappingModel !=null) {
									positionModel[rowNum][idx] 	= getValueFromArray(columnStr, idx);
									int referencePosition 		= getEcatPreference(columnStr, mappingModel.getPreferenceItem().getParamName());
									newDataMap.put(Integer.valueOf(idx), new FixedPositionModel(referencePosition, idx, true, 0, mappingModel.getPreferenceItem().getParamLang(), mappingModel.getTargetItem().getOriLang()));
								} else {
									positionModel[rowNum][idx] = columnStr[idx];
									newDataMap.put(Integer.valueOf(idx), new FixedPositionModel(idx, idx, false, 0, null, null));
								}
							}
						} else {
							for (int idx = 0; idx < standardColumnsNumber; idx++) {
								FixedPositionModel fixedPos = newDataMap.get(Integer.valueOf(idx));
								if(fixedPos.isTranslated()) {
									if(rowNum >2) {
										String translatedText = GoogleTranslator.translate(getValueFromArray(columnStr, fixedPos.getSourceLoc()), fixedPos.getEcatRefereneceLang(), fixedPos.getEcatTargetLang());
										positionModel[rowNum][fixedPos.getTargetLoc()] 	= translatedText;
									} else {
										positionModel[rowNum][fixedPos.getTargetLoc()] 	= getValueFromArray(columnStr, fixedPos.getSourceLoc());
									}
								} else {
									positionModel[rowNum][fixedPos.getTargetLoc()] = getValueFromArray(columnStr, fixedPos.getSourceLoc());
								}
							}
						}
					}
					rowNum = rowNum + 1;
				}
				cmisHelper.uploadDocument(documentFolder, tasksItem.getFileName(), getPlatString(positionModel).getBytes("UTF-8"));
			} else{
				rowNum									= 0;
				int offset								= 0;
				int standardColumnsNumber				= 0;
				while ((line = bufferedReader.readLine()) != null) {
					if(!"".equals(line)) {
						line = stringEncoding(line);
						String[] columnStr = line.split(TAB);
						if(rowNum == 0) {
							standardColumnsNumber = columnStr.length;
							int totalColumnsNumber = getTotalColumnNumber(line.split(TAB), tasksItem.getParameters().getItem(), taskModel.getTargetLangus().getItem().getLaiso(), false);
							positionModel = new String[sensorModel.getRows()][standardColumnsNumber + totalColumnsNumber];	// new data map
							for (int idx = 0; idx < standardColumnsNumber; idx++) {
								MappingModel mappingModel = subMapping.get(columnStr[idx]);
								if(mappingModel !=null) {
									if(offset >0) {
										int newOffset = idx+offset;
										positionModel[rowNum][newOffset] 		= columnStr[idx];
										int loc = isExist(columnStr, getName(mappingModel.getTargetItem()));
										if(loc ==0) {
											int targetposition 						= newOffset + 1;
											positionModel[rowNum][targetposition] 	= getName(mappingModel.getTargetItem());
											newDataMap.put(Integer.valueOf(idx), new FixedPositionModel(idx, targetposition, true, newOffset, null, null));
											offset ++;
										}else {
											newDataMap.put(Integer.valueOf(idx), new FixedPositionModel(idx, loc, true, 0, null, null));
										}
									} else {
										positionModel[rowNum][idx] 				= columnStr[idx];
										int loc = isExist(columnStr, getName(mappingModel.getTargetItem()));
										if(loc ==0) {
											int targetposition 						= idx + 1;
											positionModel[rowNum][targetposition] 	= getName(mappingModel.getTargetItem());
											newDataMap.put(Integer.valueOf(idx), new FixedPositionModel(idx, targetposition, true, 0, null, null));
											offset ++;
										} else {
											newDataMap.put(Integer.valueOf(idx), new FixedPositionModel(idx, loc, true, 0, null, null));
										}
									}
								} else {
									if(offset >0) {
										int targetposition = idx + offset;
										positionModel[rowNum][targetposition] = getValueFromArray(columnStr, idx);
										newDataMap.put(Integer.valueOf(idx), new FixedPositionModel(idx, targetposition, false, 0, null, null));
									} else {
										positionModel[rowNum][idx] = getValueFromArray(columnStr, idx);
										newDataMap.put(Integer.valueOf(idx), new FixedPositionModel(idx, idx, false, 0, null, null));
									}
								}
							}
						} else {
							for (int idx = 0; idx < standardColumnsNumber; idx++) {
								FixedPositionModel fixedPos = newDataMap.get(Integer.valueOf(idx));
								if(fixedPos.isTranslated()) {
									if(fixedPos.getPreferenceLoc() > 0) {
										String translatedText = GoogleTranslator.translate(getValueFromArray(columnStr, idx), taskModel.getReferenceLangu(), taskModel.getTargetLangus().getItem().getLaiso());
										positionModel[rowNum][fixedPos.getPreferenceLoc()] 	= getValueFromArray(columnStr, idx);
										positionModel[rowNum][fixedPos.getTargetLoc()] 		= translatedText;
									} else {
										String translatedText = GoogleTranslator.translate(getValueFromArray(columnStr, idx), taskModel.getReferenceLangu(), taskModel.getTargetLangus().getItem().getLaiso());
										positionModel[rowNum][idx] 						= getValueFromArray(columnStr, idx);
										positionModel[rowNum][fixedPos.getTargetLoc()] 	= translatedText;
									}
								} else {
									positionModel[rowNum][fixedPos.getTargetLoc()] = getValueFromArray(columnStr, fixedPos.getSourceLoc());
								}
							}
						}
					}
					rowNum = rowNum + 1;
				}
				cmisHelper.uploadDocument(documentFolder, tasksItem.getFileName(), getPlatString(positionModel).getBytes("UTF-8"));
			}
			if(sensorModel.getInputStream() !=null) {
				sensorModel.getInputStream().close();
			}
			if(bufferedReader !=null) {
				bufferedReader.close();
			}
		}
		zipfile.close();
	}
	
	private int isExist(String[] array, String str) {
		int index = 0;
		for (int idx = 0; idx < array.length; idx++) {
			if(str.equals(array[idx])) {
				index = idx;
			}
		}
		
		return index;
	}
	
	private int getEcatPreference(String[] array, String str) {
		int position = 0;
		for (int idx = 0; idx < array.length; idx++) {
			if(array[idx].equals(str)) {
				position = idx;
			}
		}
		return position;
	}

	private String getPlatString(String content[][]) {
		StringBuilder txtContents = new StringBuilder();
		StringBuilder rowContents = null;
		for (int row = 0; row < content.length; row++) {
			rowContents = new StringBuilder();
			for (int col = 0; col < content[row].length; col++) {
				rowContents.append(content[row][col]).append("	");
			}
			txtContents.append(rowContents.substring(0, rowContents.toString().length() -1)).append("\r\n");
		}
		
		return txtContents.toString().replaceAll("(?:\r\n|\n|\r)*$", "");
	}

	private String getName(ParametersItem item) {
		return "I_" + item.getParamName() + "_" + item.getParamLang(); 
	}
	
	private String getValueFromArray(String[] array, int index) {
		String value = "";
		try{
			value = array[index];
		}catch(ArrayIndexOutOfBoundsException idxException) {
			value = "";
		}
		
		return value;
	}
	
	private String stringEncoding(String str ) {
		str = StringEscapeUtils.unescapeJavaScript(str);
		return UTF2GBK.unicodeToUtf8(str);
	}
	
	private List<String> getRequiredTransatorColumn(List<ParametersItem> paraItems, String targetLang) {
		List<String> list = new ArrayList<String>();
		for (ParametersItem parametersItem : paraItems) {
			if(parametersItem.getParamLang().equals(targetLang)) {
				list.add("I_" + parametersItem.getParamName() + "_" + targetLang);
			}
		}
		
		return list;
	}
	
	private List<String> getEcatRequiredTransatorColumn(List<ParametersItem> paraItems) {
		List<String> list = new ArrayList<String>();
		for (ParametersItem parametersItem : paraItems) {
			if(parametersItem.getParamName() !=null && parametersItem.getEcatPara() !=null) {
				list.add(parametersItem.getParamName());
			}
		}
		
		return list;
	}
	
	private int getTotalColumnNumber(String[] stringArray, List<ParametersItem> paraItems, String targetLang, boolean isEcat) {
		int existColumnsNumber 	= 0;
		boolean isExist 		= false;
		if(isEcat) {
			List<String> targetColumns = getEcatRequiredTransatorColumn(paraItems);
			for (String string : targetColumns) {
				for (String subString : stringArray) {
					if(string.equals(subString)) {
						isExist = true;
					}
				}
				if(!isExist){
					existColumnsNumber ++;
					isExist = false;
				}
			}
		} else {
			List<String> targetColumns = getRequiredTransatorColumn(paraItems, targetLang);
			for (String string : targetColumns) {
				for (String subString : stringArray) {
					if(string.equals(subString)) {
						isExist = true;
					}
				}
				if(!isExist){
					existColumnsNumber ++;
					isExist = false;
				}
			}
		}
		
		return existColumnsNumber;
	}
	
	private TranslationTasksModel getMatchTasksModel(ItemIterable<CmisObject> iterator) {
		TranslationTasksModel taskModel = null;
		
		for (CmisObject cmisObject : iterator) {
			if (cmisObject instanceof Document) {
				Document document = (Document) cmisObject;
				if(document.getName().endsWith(".xml") || document.getName().endsWith(".XML")) {
					ContentStream content = document.getContentStream();
					InputStream in2 = content.getStream();
					File file = new File(content.getFileName());
					inputStreamToFile(in2, file);

					Unmarshaller jaxbUnmarshaller;
					try {
						JAXBContext jaxbContext = JAXBContext.newInstance(TranslationTasksModel.class);
						jaxbUnmarshaller = jaxbContext.createUnmarshaller();
						taskModel = (TranslationTasksModel) jaxbUnmarshaller.unmarshal(file);
					} catch (JAXBException e) {
						e.printStackTrace();
					}
				}
			}
		}
		
		return taskModel;
	}
	
	private void inputStreamToFile(InputStream inputStream, File outOfFile) {
		OutputStream outputStream = null;
	 
		try {
			// write the inputStream to a FileOutputStream
			outputStream = new FileOutputStream(outOfFile);
	 
			int read = 0;
			byte[] bytes = new byte[1024];
			while ((read = inputStream.read(bytes)) != -1) {
				outputStream.write(bytes, 0, read);
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (outputStream != null) {
				try {
					outputStream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
}
