/**
 * 
 */
package com.sap.ondemand.trans.servlet;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.chemistry.opencmis.client.api.Document;

import com.sap.ondemand.trans.helper.CmisHelper;
import com.sap.ondemand.trans.helper.GoogleTranslator;
import com.sap.ondemand.trans.helper.GsonHelper;
import com.sap.ondemand.trans.helper.LocalHelper;

/**
 * @author I075006
 * 
 */
public class PostServiceServlet extends HttpServlet {

	private static final long serialVersionUID = -8661072563213021498L;
	private CmisHelper cmisHelper = new CmisHelper();
	public static final String TAB = "	";
	private static final String SUCCESS = "Successfully";
	private Map<String, String> localMap = null;
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * javax.servlet.http.HttpServlet#doPost(javax.servlet.http.HttpServletRequest
	 * , javax.servlet.http.HttpServletResponse)
	 */
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html;charset=utf-8");
		response.setHeader("Cache-Control", "no-cache");
		PrintWriter out 		= response.getWriter();
		String status 			= "";
		String action			= req.getParameter("action");
		String relationLocation = req.getParameter("relationLocation");
		String language 		= req.getParameter("newLanguage");
		String description 		= req.getParameter("description");
		String reference 		= req.getParameter("reference");
		String documnetId 		= req.getParameter("documentId");
		// ===================
		String fromLang 		= req.getParameter("fromLang");
		String toLang 			= req.getParameter("toLang");
		String source 			= req.getParameter("source");
		
		if(reference == null) {
			reference = "";
		}
		if(action.equals("addLanguage")) {
			status = addNewLanguage(documnetId, relationLocation, language, description, reference);
		} else if(action.equals("doTranslation")) {
			if(localMap == null) {
				localMap = LocalHelper.getLocalInfo();
			}
			try {
				translator(fromLang, toLang, relationLocation, source, documnetId);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		out.print(GsonHelper.toJson(status));
		out.flush();
		out.close();
	}
	
	public String addNewLanguage(String documnetId, String relationLoc, String language, String description, String reference) {
		int relationIndex 	= 0;
		int rowIndex 		= 0;
		int splitIndex 		= 0;
		try {
			Document document 				= cmisHelper.getDocumentById(documnetId);
			InputStream inputStream 		= document.getContentStream().getStream();
			BufferedReader bufferedReader 	= new BufferedReader(new InputStreamReader(inputStream));
			String line;
			StringBuffer fileContents = new StringBuffer();
			while ((line = bufferedReader.readLine()) != null) {
				StringBuffer newHeaderData = new StringBuffer();
				String[] splitHeader = line.split(TAB);
				if (rowIndex == 0) {
					splitIndex = splitHeader.length;
					for (int i = 0; i < splitHeader.length; i++) {
						if (splitHeader[i].trim().equals(relationLoc)) {
							newHeaderData.append(splitHeader[i]).append(TAB).append(language).append(TAB);
							relationIndex = i;
						} else {
							newHeaderData.append(splitHeader[i]).append(TAB);
						}
					}
					fileContents.append(newHeaderData.substring(0, newHeaderData.length() - 1).toString()).append("\r\n");
				} else if(rowIndex == 1) {
					for (int i = 0; i < splitIndex; i++) {
						String temp = "";
						try{
							temp =splitHeader[i];
						} catch(Exception ex) {
							temp = "";
						}
						if(i == relationIndex) {
							newHeaderData.append(temp).append(TAB).append(description).append(TAB);
						} else {
							newHeaderData.append(temp).append(TAB);
						}
					}
					fileContents.append(newHeaderData.substring(0, newHeaderData.length() - 1).toString()).append("\r\n");
				}else if(rowIndex == 2) {
					for (int i = 0; i < splitIndex; i++) {
						String temp = "";
						try{
							temp =splitHeader[i];
						} catch(Exception ex) {
							temp = "";
						}
						if(i == relationIndex) {
							newHeaderData.append(temp).append(TAB).append(reference).append(TAB);
						} else {
							newHeaderData.append(temp).append(TAB);
						}
					}
					fileContents.append(newHeaderData.substring(0, newHeaderData.length() - 1).toString()).append("\r\n");
				} else {
					for (int i = 0; i < splitIndex; i++) {
						String temp = "";
						try{
							temp =splitHeader[i];
						} catch(Exception ex) {
							temp = "";
						}
						if(i == relationIndex) {
							newHeaderData.append(temp).append(TAB).append("").append(TAB);
						} else {
							newHeaderData.append(temp).append(TAB);
						}
					}
					fileContents.append(newHeaderData.substring(0, newHeaderData.length() - 1).toString()).append("\r\n");
				}
				rowIndex++;
			}
			inputStream.close();
//			cmisHelper.updateDoument(document, document.getName(), fileContents.toString().getBytes());
			return SUCCESS;
		} catch (Exception e) {
			return e.getMessage();
		}
	}
	
	private void translator(String fromLang, String toLang,String relationLocation,String source, String documnetId) throws Exception {
		int rowIndex 		= 0;
		int splitIndex 		= 0;
		int fromIndex		= 0;
		int toIndex			= 0;
		Document document 				= cmisHelper.getDocumentById(documnetId);
		InputStream inputStream 		= document.getContentStream().getStream();
		BufferedReader bufferedReader 	= new BufferedReader(new InputStreamReader(inputStream));
		String line;
		StringBuffer fileContents = new StringBuffer();
		while ((line = bufferedReader.readLine()) != null) {
			StringBuffer newHeaderData = new StringBuffer();
			String[] splitHeader = line.split(TAB);
			if (rowIndex <= 2) {
				if(rowIndex == 0) {
					splitIndex = splitHeader.length;
					for (int i = 0; i < splitHeader.length; i++) {
						if (splitHeader[i].trim().equals(relationLocation)) {
							fromIndex = i;
						} else if(splitHeader[i].trim().equals(source)) {
							toIndex	= i;
						}
					}
				}
				fileContents.append(line).append("\r\n");
			} else {
				for (int i = 0; i < splitIndex; i++) {
					String temp = "";
					if(i == toIndex) {
						try{
							temp =splitHeader[fromIndex];
						} catch(Exception ex) {
							temp = "";
						}
						String translatedText =GoogleTranslator.translate(temp, localMap.get(fromLang), localMap.get(toLang));
//						String translatedText = MicrosoftTranslationHelper.translator(temp, localMap.get(fromLang), localMap.get(toLang));
						translatedText = new String(translatedText.getBytes("UTF-8"));
						newHeaderData.append(translatedText).append(TAB);
					} else {
						try{
							temp =splitHeader[i];
						} catch(Exception ex) {
							temp = "";
						}
						newHeaderData.append(temp).append(TAB);
					}
				}
				fileContents.append(newHeaderData.substring(0, newHeaderData.length() - 1).toString()).append("\r\n");
			}
			rowIndex++;
		}
		inputStream.close();
//		cmisHelper.updateDoument(document, document.getName(), fileContents.toString().getBytes());
	}
	
//	private String deleteDocumentByDocumentId(String documentId) {
//		cmisHelper.deleteDocumentByDocumentId(documentId);
//		return SUCCESS;
//	}
	
//	private void startTranslation(String documentId) {
//		Folder documentFolder = cmisHelper.getFolderOfApp(getFileName(documentId));
//		ItemIterable<CmisObject> cmisIterator = documentFolder.getChildren();
//		for (CmisObject cmisObject : cmisIterator) {
//			if (cmisObject instanceof Document) {
//				Document document = (Document) cmisObject;
//				ContentStream content = document.getContentStream();
//				InputStream in2 = content.getStream();
//				File file = new File(content.getFileName());
//				inputStreamToFile(in2, file);
//
//				Unmarshaller jaxbUnmarshaller;
//				try {
//					JAXBContext jaxbContext = JAXBContext.newInstance(TranslationTasksModel.class);
//					jaxbUnmarshaller = jaxbContext.createUnmarshaller();
//					TranslationTasksModel objInstance = (TranslationTasksModel) jaxbUnmarshaller.unmarshal(file);
//				} catch (JAXBException e) {
//					e.printStackTrace();
//				}
//			}
//		}
//	}
	
//	private void inputStreamToFile(InputStream inputStream, File outOfFile) {
//		OutputStream outputStream = null;
//	 
//		try {
//			// write the inputStream to a FileOutputStream
//			outputStream = new FileOutputStream(outOfFile);
//	 
//			int read = 0;
//			byte[] bytes = new byte[1024];
//			while ((read = inputStream.read(bytes)) != -1) {
//				outputStream.write(bytes, 0, read);
//			}
//		} catch (IOException e) {
//			e.printStackTrace();
//		} finally {
//			if (outputStream != null) {
//				try {
//					// outputStream.flush();
//					outputStream.close();
//				} catch (IOException e) {
//					e.printStackTrace();
//				}
//			}
//		}
//	}
//	
//	private String getFileName(String filename) {
//		String name = filename.substring(0,filename.lastIndexOf("."));
//		return name;
//	}
}
