/**
 * 
 */
package com.sap.ondemand.trans.servlet;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.JAXBException;

import org.apache.chemistry.opencmis.client.api.Folder;

import com.sap.ondemand.trans.files.FileOperations;
import com.sap.ondemand.trans.helper.CmisHelper;

/**
 * @author I075006
 * 
 */
public class Upload extends HttpServlet {

	private static final long serialVersionUID = -1361116115617940868L;
	private CmisHelper cmisHelper;
	private String uploadFileName;
	
	/* (non-Javadoc)
	 * @see javax.servlet.http.HttpServlet#doPost(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse resp)throws ServletException, IOException {
		uploadFileName = request.getParameter("file_name");
		try {
			cmisHelper = new CmisHelper();
			File file = getUploadFile(request);
			Folder folder = cmisHelper.getFolderOfApp(getFileName(uploadFileName));
			cmisHelper.addDocument(file, folder);
			
			splitZipFile(file, folder);
		} catch (NamingException e1) {
			e1.printStackTrace();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (JAXBException e) {
			e.printStackTrace();
		}
	}
	
	private File getUploadFile(HttpServletRequest request) throws IOException {
		int len = request.getContentLength();
		byte buffer[] = new byte[len];
		File tempFile = new File(uploadFileName);
		InputStream in = request.getInputStream();
		int total = 0;
		int once = 0;
		while ((total < len) && (once >= 0)) {
			once = in.read(buffer, total, len);
			total += once;
		}

		OutputStream out = new BufferedOutputStream(new FileOutputStream(tempFile, true));
		byte[] breaker = "\r\ntranslation: -------------------->\r\n".getBytes();
		out.write(breaker, 0, breaker.length);
		out.write(buffer);
		out.close();
		in.close();
		
		return tempFile;
	}
	
	private void splitZipFile(File zipFile, Folder folder) throws IOException, NamingException, InstantiationException, IllegalAccessException, JAXBException {
		FileOperations fileOperation = FileOperations.getInstance();
		fileOperation.contentMatcher(zipFile, folder);
	}
	
	private String getFileName(String filename) {
		String name = filename.substring(0,filename.lastIndexOf("."));
		return name;
	}
}
