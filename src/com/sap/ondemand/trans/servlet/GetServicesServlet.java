/**
 * 
 */
package com.sap.ondemand.trans.servlet;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.apache.chemistry.opencmis.client.api.CmisObject;
import org.apache.chemistry.opencmis.client.api.Document;
import org.apache.chemistry.opencmis.client.api.Folder;
import org.apache.chemistry.opencmis.client.api.ItemIterable;
import org.apache.chemistry.opencmis.commons.data.ContentStream;
import org.apache.chemistry.opencmis.commons.exceptions.CmisObjectNotFoundException;
import org.apache.chemistry.opencmis.commons.impl.IOUtils;
import org.apache.commons.lang.StringEscapeUtils;

import com.sap.ondemand.trans.files.model.DocumentHeaderModel;
import com.sap.ondemand.trans.files.model.LanguageModel;
import com.sap.ondemand.trans.files.model.LanguagesModel;
import com.sap.ondemand.trans.helper.CmisHelper;
import com.sap.ondemand.trans.helper.GsonHelper;
import com.sap.ondemand.trans.helper.LocalHelper;
import com.sap.ondemand.trans.helper.TargetTranslatorFile;
import com.sap.ondemand.trans.helper.UTF2GBK;
import com.sap.ondemand.trans.jaxb.model.ParametersItem;
import com.sap.ondemand.trans.jaxb.model.TasksItem;
import com.sap.ondemand.trans.jaxb.model.TranslationTasksModel;
import com.sap.ondemand.trans.jaxb.model.VariantMappingModel;
import com.sap.ondemand.trans.jaxb.model.ZipObjectModel;
import com.sap.ondemand.trans.services.TranslatedModel;
import com.sap.ondemand.trans.services.UploadFilesModel;
import com.sap.ondemand.trans.services.VariantFileModel;

/**
 * @author I075006
 * 
 */
public class GetServicesServlet extends HttpServlet {

	private static final long serialVersionUID = -4103701814899527211L;
	private CmisHelper cmisHelper = new CmisHelper();

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * javax.servlet.http.HttpServlet#doGet(javax.servlet.http.HttpServletRequest
	 * , javax.servlet.http.HttpServletResponse)
	 */
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse response) throws ServletException, IOException {
		String action = req.getParameter("action");
		response.setContentType("text/html;charset=utf-8");
		response.setHeader("Cache-Control", "no-cache");
		PrintWriter out = response.getWriter();
		String documentId = req.getParameter("documentId");
		String variantPkg = req.getParameter("pkg");
		if (action.equals("uploadData")) {
			try {
//				 cmisHelper.deleteAllDocument();
				out.print(readUploadData());
			} catch (NamingException e) {
				e.printStackTrace();
			}
		}else if (action.equals("variantDetails")) {
			try {
				out.print(GsonHelper.toJson(getVariantDetails(documentId)));
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else if (action.equals("detail")) {
			try {
				out.print(getVariantDetail(variantPkg, documentId));
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else if (action.equals("headers")) {
			try {
				out.print(getDocumentHeader(documentId));
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else if (action.equals("download")) {
			try {
				downloadDocument(documentId, response);
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else if (action.equals("translationDlg")) {
			try {
				out.print(GsonHelper.toJson(getLanguages(documentId)));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		out.flush();
		out.close();
	}
	
	private String readUploadData() throws NamingException {
		List<UploadFilesModel> uploadFiles 	= new ArrayList<UploadFilesModel>();
		TranslationTasksModel tasksModel 	= null;
		// Get root folder from CMIS session
		Folder rootFolder = cmisHelper.getRootFolderOfRepository();
		// List content of root folder
		ItemIterable<CmisObject> children 	= rootFolder.getChildren();
		Iterator<CmisObject> iter 			= children.iterator();
		Iterator<CmisObject> subIter = null;
		Object subCmisObject = null;
		while (iter.hasNext()) {
			Object cmisObject = iter.next();
			if (cmisObject instanceof Folder) {
				Folder folder = (Folder) cmisObject;
				subIter = folder.getChildren().iterator();
				while (subIter.hasNext()) {
					subCmisObject = subIter.next();
					if (subCmisObject instanceof Document) {
						Document document = (Document) subCmisObject;
						if (document.getName().contains(".zip") || document.getName().contains(".ZIP")) {
							tasksModel = TargetTranslatorFile.getMatchTasksModel(folder.getChildren());
							UploadFilesModel uploadFileModel = new UploadFilesModel();
							uploadFileModel.setFileName(folder.getName());
							uploadFileModel.setImage("./././components/img/zipfile.jpg");
							uploadFileModel.setReferenceLanguage(tasksModel.getReferenceLangu());
							uploadFileModel.setTargetLanguage(tasksModel.getTargetLangus().getItem().getLaiso());
							uploadFiles.add(uploadFileModel);
						}
					}
				}
			}
		}

		return GsonHelper.toJson(uploadFiles);
	}
	
	public List<VariantFileModel> getVariantDetails(String variantName) throws Exception {
		List<VariantFileModel> variants = null;
		// Get root folder from CMIS session
//		Folder rootFolder = cmisHelper.getRootFolderOfRepository();
		ZipObjectModel objectModel 	= cmisHelper.getFolderProperties(variantName);
		Folder rootFolder 			= cmisHelper.getFolderById(objectModel.getObjectId());
//		ItemIterable<CmisObject> children 	= rootFolder.getChildren();
//		Iterator<CmisObject> iter 			= children.iterator();
//		while (iter.hasNext()) {
//			Object cmisObject = iter.next();
//			if (cmisObject instanceof Folder) {
//				Folder folder = (Folder) cmisObject;
//				if(folder.getName().equalsIgnoreCase(variantName)) {
					variants = getTranslatedStatus(rootFolder);
//				}
//			}
//		}
		
		return variants;
	}
	
	private List<VariantFileModel> getTranslatedStatus(Folder folder) {
		List<VariantFileModel> variantFiles = new ArrayList<VariantFileModel>();
		Map<String, TranslatedModel> rootFiles 		= getFileMapping(folder.getChildren());
		TranslationTasksModel tasksModel 	= TargetTranslatorFile.getMatchTasksModel(folder.getChildren());
		List<TasksItem> taskItems 			= tasksModel.getTasks().getItem();
		TranslatedModel exist = null;
		for (TasksItem tasksItem : taskItems) {
			if(tasksItem.getObjty().equals("ECAT")) {
				System.out.println("");
			}
			exist = rootFiles.get(tasksItem.getFileName());
			VariantFileModel variantFileModel = new VariantFileModel();
			variantFileModel.setFileName(tasksItem.getFileName());
			variantFileModel.setTypeImage("./././components/img/Notebook.png");
			variantFileModel.setObjType(tasksItem.getObjty());
			if(exist != null) {
				variantFileModel.setTranslated("Yes");
				variantFileModel.setTranslatedDate(exist.getModifyDate());
			} else {
				variantFileModel.setTranslated("No");
			}
			variantFiles.add(variantFileModel);
		}
		return variantFiles;
	}
	
	private Map<String, TranslatedModel> getFileMapping(ItemIterable<CmisObject> children) {
		Map<String, TranslatedModel> filemap = new HashMap<String, TranslatedModel>();
		Iterator<CmisObject> iter = children.iterator();
		while (iter.hasNext()) {
			Object cmisObject = iter.next();
			if (cmisObject instanceof Document) {
				Document document = (Document) cmisObject;
				Calendar calendar = document.getCreationDate();
				filemap.put(document.getName(), new TranslatedModel(document.getName(), calendar.getTime().toString()));
			}
		}
		
		return filemap;
	}

	private String getDocumentHeader(String documentId) throws Exception {
		Document document = cmisHelper.getDocumentById(documentId);
		ContentStream contentStream = document.getContentStream();
		InputStream inputStream = contentStream.getStream();
		BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
		List<DocumentHeaderModel> list = new ArrayList<DocumentHeaderModel>();

		String line = bufferedReader.readLine();
		String[] headers = line.split("	");
		for (String headerName : headers) {
			list.add(new DocumentHeaderModel(headerName, true));
		}
		return GsonHelper.toJson(list);
	}

	private LanguagesModel getLanguages(String documentId) throws Exception {
		LanguagesModel languages = new LanguagesModel();
		Document document = cmisHelper.getDocumentById(documentId);
		ContentStream contentStream = document.getContentStream();
		InputStream inputStream = contentStream.getStream();
		BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
		List<DocumentHeaderModel> list = new ArrayList<DocumentHeaderModel>();
		List<DocumentHeaderModel> needTrans = new ArrayList<DocumentHeaderModel>();

		String line = bufferedReader.readLine();
		String[] headers = line.split("	");
		for (String headerName : headers) {
			list.add(new DocumentHeaderModel(headerName, true));
			if (matcher(headerName.trim())) {
				needTrans.add(new DocumentHeaderModel(headerName, true));
			}
		}
		languages.setRelatives(list);
		languages.setNeedToTransColumn(needTrans);

		languages.setLanguages(getLocalMap());
		return languages;
	}

	private boolean matcher(String row) {
		String REGEX = "_\\d\\s|_[A-Z]{2}+$";
		Pattern PATTERN = Pattern.compile(REGEX);
		Matcher m = PATTERN.matcher(row);
		boolean result = m.find();
		return result;
	}

	public List<LanguageModel> getLocalMap() {
		List<LanguageModel> languages = new ArrayList<LanguageModel>();
		Map<String, String> localMap = LocalHelper.getLocalInfo();
		Set<String> keys = localMap.keySet();
		for (String language : keys) {
			languages.add(new LanguageModel(language, true));
		}

		return languages;
	}

	private String getVariantDetail(String variantFile, String translatedFile) throws Exception {
		String jsonData = "";
		ZipEntry entry 	= null;
		ZipFile zipfile = null;
		InputStream variantFileStream = null;
		VariantMappingModel mappintModel = null;
		try{
			ZipObjectModel documentPro = cmisHelper.getDocumentProperties(translatedFile);
			ZipObjectModel objectModel 				= cmisHelper.getFolderProperties(variantFile);
			Folder rootFolder 						= cmisHelper.getFolderById(objectModel.getObjectId());
			ItemIterable<CmisObject> cmisIterator 	= rootFolder.getChildren();
			TranslationTasksModel taskModel 		= getMatchTasksModel(cmisIterator);
			mappintModel 		= getVariantMapping(taskModel, translatedFile);
			
			if(documentPro !=null) {
				Document document 						= cmisHelper.getDocumentById(documentPro.getObjectId());
				jsonData = getSourceFileContent(document.getContentStream().getStream(), mappintModel, "UTF-8", true);
			} else {
				File variantPkg = cmisHelper.getPackage(variantFile, translatedFile);
				zipfile = new ZipFile(variantPkg);
				Enumeration<? extends ZipEntry> enumeration = zipfile.entries();
				while (enumeration.hasMoreElements()) {
					entry = (ZipEntry) enumeration.nextElement();
					if(entry.getName().equals(translatedFile)) {
						variantFileStream = zipfile.getInputStream(entry);
						jsonData = getSourceFileContent(variantFileStream, mappintModel, "unicode", false);
					}
				}
			}
		}catch(CmisObjectNotFoundException notFound) {
			File variantPkg = cmisHelper.getPackage(variantFile, translatedFile);
			zipfile = new ZipFile(variantPkg);
			Enumeration<? extends ZipEntry> enumeration = zipfile.entries();
			while (enumeration.hasMoreElements()) {
				entry = (ZipEntry) enumeration.nextElement();
				if(entry.getName().equals(translatedFile)) {
					variantFileStream = zipfile.getInputStream(entry);
					jsonData = getSourceFileContent(variantFileStream, mappintModel, "unicode", false);
				}
			}
		}finally{
			if(zipfile!=null) {
				zipfile.close();
			}
		}
		
		return jsonData;
	}
	
//	private Folder getFolder (String variantName) {
//		Folder rootFolder = cmisHelper.getRootFolderOfRepository();
//		ItemIterable<CmisObject> cmisIterator = rootFolder.getChildren();
//		for (CmisObject cmisObject : cmisIterator) {
//			if((cmisObject instanceof Folder) && cmisObject.getName().equals(variantName)) {
//				Folder fo = (Folder)cmisObject;
//				return fo;
//			}
//		}
//		return null;
//	}
	
	private VariantMappingModel getVariantMapping(TranslationTasksModel tasksModel, String variantFile) {
		List<String> referenceColumns 	= new ArrayList<String>();
		List<String> targetColumns 		= new ArrayList<String>();
		String referenceLang 			= tasksModel.getReferenceLangu();
		String targetLang				= tasksModel.getTargetLangus().getItem().getLaiso();
		VariantMappingModel mappingModel = new VariantMappingModel();
		
		List<TasksItem> taskList = tasksModel.getTasks().getItem();
		for (TasksItem tasksItem : taskList) {
			if(tasksItem.getFileName().equals(variantFile)) {
				List<ParametersItem> paraItems = tasksItem.getParameters().getItem();
				if(tasksItem.getObjty().equals("ECAT")) {
					for (ParametersItem parametersItem : paraItems) {
						if(parametersItem.getEcatPara() != null) {
							targetColumns.add(parametersItem.getParamName());
							referenceColumns.add(parametersItem.getEcatPara());
						}
					}
				} else {
					for (ParametersItem parametersItem : paraItems) {
						if(parametersItem.getParamLang().equals(referenceLang)) {
							referenceColumns.add("I_" + parametersItem.getParamName() + "_" + parametersItem.getParamLang());
						} else if(parametersItem.getParamLang().equals(targetLang)) {
							targetColumns.add("I_" + parametersItem.getParamName() + "_" + parametersItem.getParamLang());
						}
					}
				}
			}
		}
		removeDuplicate(targetColumns);
		removeDuplicate(referenceColumns);
		mappingModel.setReferenceColumns(referenceColumns);
		mappingModel.setTargetColumns(targetColumns);
		
		return mappingModel;
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void removeDuplicate(List arlList){ 
	   HashSet h = new HashSet(arlList);  
	   arlList.clear();  
	   arlList.addAll(h);  
	} 
	
	private TranslationTasksModel getMatchTasksModel(ItemIterable<CmisObject> iterator) {
		TranslationTasksModel taskModel = null;
		
		for (CmisObject cmisObject : iterator) {
			if (cmisObject instanceof Document) {
				Document document = (Document) cmisObject;
				if(document.getName().endsWith(".xml") || document.getName().endsWith(".XML")) {
					ContentStream content = document.getContentStream();
					InputStream in2 = content.getStream();
					File file = new File(content.getFileName());
					inputStreamToFile(in2, file);

					Unmarshaller jaxbUnmarshaller;
					try {
						JAXBContext jaxbContext = JAXBContext.newInstance(TranslationTasksModel.class);
						jaxbUnmarshaller = jaxbContext.createUnmarshaller();
						taskModel = (TranslationTasksModel) jaxbUnmarshaller.unmarshal(file);
					} catch (JAXBException e) {
						e.printStackTrace();
					}
				}
			}
		}
		
		return taskModel;
	}
	
	private void inputStreamToFile(InputStream inputStream, File outOfFile) {
		OutputStream outputStream = null;
	 
		try {
			// write the inputStream to a FileOutputStream
			outputStream = new FileOutputStream(outOfFile);
	 
			int read = 0;
			byte[] bytes = new byte[1024];
			while ((read = inputStream.read(bytes)) != -1) {
				outputStream.write(bytes, 0, read);
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (outputStream != null) {
				try {
					outputStream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	private String getSourceFileContent(InputStream inputStream, VariantMappingModel mappintModel, String encoding, boolean status) throws IOException {
		BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, encoding));

		String data, columns, rows;
		String line = bufferedReader.readLine();
		if(encoding.equals("unicode")) {
			line = stringEncoding(line);
		}
		StringBuffer headeBuffer = new StringBuffer();

		headeBuffer.append("[");
		String[] headers = line.split("	");
		for (String string : headers) {
			headeBuffer.append("{").append("\"column\"").append(":").append("\"" + string + "\"");
			if(mappintModel.getReferenceColumns().contains(string)) {
				headeBuffer.append(",").append("\"iReferenceColumn\"").append(":").append("\"" + true + "\"");
				headeBuffer.append(",").append("\"iTtargetColumn\"").append(":").append("\"" + false + "\"");
			} else if(mappintModel.getTargetColumns().contains(string) && status) {
				headeBuffer.append(",").append("\"iReferenceColumn\"").append(":").append("\"" + false + "\"");
				headeBuffer.append(",").append("\"iTtargetColumn\"").append(":").append("\"" + true + "\"");
			} else {
				headeBuffer.append(",").append("\"iReferenceColumn\"").append(":").append("\"" + false + "\"");
				headeBuffer.append(",").append("\"iTtargetColumn\"").append(":").append("\"" + false + "\"");
			}
			headeBuffer.append("}").append(",");
		}
		columns = headeBuffer.substring(0, headeBuffer.length() - 1) + "]";

		rows = "[";
		while ((data = bufferedReader.readLine()) != null) {
			if(!"".equals(data)) {
				if(encoding.equals("unicode")) {
					data = stringEncoding(data);
				}
				String[] rowSplits = data.split("	");
				rows += "{";
				StringBuffer rowsBuffer = new StringBuffer();
				for (int i = 0; i < headers.length; i++) {
					try {
						if (rowSplits[i] == null || "".equals(rowSplits[i])) {
							rowsBuffer.append("\"" + headers[i] + "\"").append(":").append("\"" + " " + "\"").append(",");
						} else {
							rowsBuffer.append("\"" + headers[i] + "\"").append(":").append("\"" + rowSplits[i] + "\"").append(",");
						}
					} catch (Exception ex) {
						rowsBuffer.append("\"" + headers[i] + "\"").append(":").append("\"" + " " + "\"").append(",");
					}
				}
				rows += rowsBuffer.substring(0, rowsBuffer.length() - 1) + "},";
			}
		}
		if (rows.equals("[")) {
			rows = "[]";
		} else {
			rows = rows.substring(0, rows.length() - 1) + "]";
		}
		
		bufferedReader.close();
		inputStream.close();
		return "{\"columns\":" + columns + ",\"rows\":" + rows + "}";
	}
	
	private String stringEncoding(String str ) {
		str = StringEscapeUtils.unescapeJavaScript(str);
		return UTF2GBK.unicodeToUtf8(str);
	}
	
//	private String getSourceFileContent(Document document) throws IOException {
//		ContentStream contentStream = document.getContentStream();
//		InputStream inputStream = contentStream.getStream();
//		BufferedReader bufferedReader = new BufferedReader(
//				new InputStreamReader(inputStream, "UTF-8"));
//
//		String data, columns, rows;
//		String line = bufferedReader.readLine();
//		StringBuffer headeBuffer = new StringBuffer();
//
//		headeBuffer.append("[");
//		String[] headers = line.split("	");
//		for (String string : headers) {
//			headeBuffer.append("{").append("\"column\"").append(":")
//					.append("\"" + string + "\"").append("}").append(",");
//		}
//		columns = headeBuffer.substring(0, headeBuffer.length() - 1) + "]";
//
//		rows = "[";
//		while ((data = bufferedReader.readLine()) != null) {
//			String[] rowSplits = data.split("	");
//			rows += "{";
//			StringBuffer rowsBuffer = new StringBuffer();
//			for (int i = 0; i < headers.length; i++) {
//				try {
//					if (rowSplits[i] == null || "".equals(rowSplits[i])) {
//						rowsBuffer.append("\"" + headers[i] + "\"").append(":")
//								.append("\"" + " " + "\"").append(",");
//					} else {
//						rowsBuffer.append("\"" + headers[i] + "\"").append(":")
//								.append("\"" + rowSplits[i] + "\"").append(",");
//					}
//				} catch (Exception ex) {
//					rowsBuffer.append("\"" + headers[i] + "\"").append(":")
//							.append("\"" + " " + "\"").append(",");
//				}
//			}
//			rows += rowsBuffer.substring(0, rowsBuffer.length() - 1) + "},";
//		}
//		if (rows.equals("[")) {
//			rows = "[]";
//		} else {
//			rows = rows.substring(0, rows.length() - 1) + "]";
//		}
//
//		return "{\"columns\":" + columns + ",\"rows\":" + rows + "}";
//	}

//	private String getTranslatedFile(Document selectedDocument)
//			throws Exception {
//		// Document translatedDoc = null;
//		// List<Folder> folders = selectedDocument.getParents();
//		// Folder folder = folders.get(0);
//
//		ZipObjectModel zipObjectModel = cmisHelper
//				.queryDocument(getFileName(selectedDocument.getName())
//						+ "_translated.TXT");
//		Document translatedDoc = cmisHelper.getDocumentById(zipObjectModel
//				.getObjectId());
//		// Iterator<CmisObject> iter = folder.getChildren().iterator();
//		// Object cmisObject;
//		// while (iter.hasNext()) {
//		// cmisObject = iter.next();
//		// if (cmisObject instanceof Document) {
//		// Document document = (Document) cmisObject;
//		// if(document.getName().equals(getFileName(selectedDocument.getName())
//		// + "_translated.TXT")) {
//		// translatedDoc = document;
//		// }
//		// }
//		// }
//
//		ContentStream contentStream = translatedDoc.getContentStream();
//		InputStream inputStream = contentStream.getStream();
//		BufferedReader bufferedReader = new BufferedReader(
//				new InputStreamReader(inputStream, "UTF-8"));
//
//		String data, columns, rows;
//		String line = bufferedReader.readLine();
//		StringBuffer headeBuffer = new StringBuffer();
//
//		headeBuffer.append("[");
//		String[] headers = line.split("	");
//		for (String string : headers) {
//			headeBuffer.append("{").append("\"column\"").append(":")
//					.append("\"" + string + "\"").append("}").append(",");
//		}
//		columns = headeBuffer.substring(0, headeBuffer.length() - 1) + "]";
//
//		rows = "[";
//		while ((data = bufferedReader.readLine()) != null) {
//			String[] rowSplits = data.split("	");
//			rows += "{";
//			StringBuffer rowsBuffer = new StringBuffer();
//			for (int i = 0; i < headers.length; i++) {
//				try {
//					if (rowSplits[i] == null || "".equals(rowSplits[i])) {
//						rowsBuffer.append("\"" + headers[i] + "\"").append(":")
//								.append("\"" + " " + "\"").append(",");
//					} else {
//						rowsBuffer.append("\"" + headers[i] + "\"").append(":")
//								.append("\"" + rowSplits[i] + "\"").append(",");
//					}
//				} catch (Exception ex) {
//					rowsBuffer.append("\"" + headers[i] + "\"").append(":")
//							.append("\"" + " " + "\"").append(",");
//				}
//			}
//			rows += rowsBuffer.substring(0, rowsBuffer.length() - 1) + "},";
//		}
//		if (rows.equals("[")) {
//			rows = "[]";
//		} else {
//			rows = rows.substring(0, rows.length() - 1) + "]";
//		}
//
//		return "{\"columns\":" + columns + ",\"rows\":" + rows + "}";
//	}

//	private String getFileName(String filename) {
//		String name = filename.substring(0, filename.lastIndexOf("."));
//		return name;
//	}

	private OutputStream downloadDocument(String documentId,HttpServletResponse response) throws Exception {
		try {
			ContentStream docStream = cmisHelper.downloadDocumentByDocumentId(documentId);
			if (docStream != null) {
				response.setContentType(docStream.getMimeType());
				IOUtils.copy(docStream.getStream(), response.getOutputStream());
				IOUtils.closeQuietly(docStream.getStream());
			} else { // If the document doesn't have any stream return "file-not-found" status code in http responce
				response.setStatus(404);
			}
		} catch (Exception e) {
			throw new Exception(e);
		}
		return null;
	}
}
