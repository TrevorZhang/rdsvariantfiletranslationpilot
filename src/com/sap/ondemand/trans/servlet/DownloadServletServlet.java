/**
 * 
 */
package com.sap.ondemand.trans.servlet;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.chemistry.opencmis.client.api.CmisObject;
import org.apache.chemistry.opencmis.client.api.Document;
import org.apache.chemistry.opencmis.client.api.Folder;
import org.apache.chemistry.opencmis.client.api.ItemIterable;
import org.apache.chemistry.opencmis.commons.data.ContentStream;
import org.apache.chemistry.opencmis.commons.impl.IOUtils;

import com.sap.ondemand.trans.helper.CmisHelper;
import com.sap.ondemand.trans.jaxb.model.ZipObjectModel;

/**
 * @author I075006
 * 
 */
public class DownloadServletServlet extends HttpServlet {

	private static final long serialVersionUID = 7569687860170086202L;
	private CmisHelper cmisHelper = new CmisHelper();

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * javax.servlet.http.HttpServlet#doGet(javax.servlet.http.HttpServletRequest
	 * , javax.servlet.http.HttpServletResponse)
	 */
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String documentId = req.getParameter("documentId");
		try {
			downloadDocument(documentId, resp);
		} catch (Exception e) {
			throw new IOException(e.getMessage());
		}
	}

	private void downloadDocument(String documentId, HttpServletResponse response) throws Exception {
		try {
			ZipObjectModel objectModel 	= cmisHelper.getDocumentProperties(documentId + ".zip");
			ContentStream docStream 	= cmisHelper.downloadDocumentByDocumentId(objectModel.getObjectId());
			if (docStream.getFileName().contains(".ZIP") || docStream.getFileName().contains(".zip")) {
				Folder folder = cmisHelper.getFolderOfApp(documentId);
				ItemIterable<CmisObject> cmisIterator = folder.getChildren();
				// create ZipOutputStream to write to the zip file
				response.addHeader("Content-Disposition", "attachment;filename=" + docStream.getFileName());
				response.setContentType("application/octet-stream");
				OutputStream out 	= response.getOutputStream();
				ZipOutputStream zos = new ZipOutputStream(out);
				for (CmisObject cmisObject : cmisIterator) {
					if (cmisObject.getName().contains(".txt") || cmisObject.getName().contains(".TXT")) {
						Document doc = (Document) cmisObject;
						// add a new Zip Entry to the ZipOutputStream
						ZipEntry ze 			= new ZipEntry(doc.getName());
						zos.putNextEntry(ze);
						// read the file and write to ZipOutputStream
						InputStream fis = doc.getContentStream().getStream();
						byte[] buffer = new byte[1024];
						int len;
						while ((len = fis.read(buffer)) > 0) {
							zos.write(buffer, 0, len);
						}
						// Close the zip entry to write to zip file
						zos.closeEntry();
						fis.close();
					}
				}
				// Close resources
				zos.close();
				out.close();
			} else {
				if (docStream != null) {
					response.addHeader("Content-Disposition", "attachment;filename=" + docStream.getFileName());
					response.setContentType(docStream.getMimeType());
					IOUtils.copy(docStream.getStream(), response.getOutputStream());
					IOUtils.closeQuietly(docStream.getStream());
				}
			}
		} catch (Exception e) {
			throw new Exception(e);
		}
	}
}
