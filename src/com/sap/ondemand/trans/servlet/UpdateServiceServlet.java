/**
 * 
 */
package com.sap.ondemand.trans.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 * @author I075006
 *
 */
public class UpdateServiceServlet extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = -177406075462435024L;

	/* (non-Javadoc)
	 * @see javax.servlet.http.HttpServlet#doPost(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		resp.setContentType("text/html;charset=utf-8");
		resp.setHeader("Cache-Control", "no-cache");
		String status	= "";
		String updateData 	= req.getParameter("updateData");
		PrintWriter out 	= resp.getWriter();
		System.out.println("data=====" + updateData);
		
		JSONParser parser = new JSONParser();
		Object obj;
		try {
			String test =  JSONObject.escape(updateData.substring(1, updateData.length() - 1));
			obj = parser.parse(test);
			JSONObject jsonObject = (JSONObject) obj;
			System.out.println(jsonObject);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
//		out.print(GsonHelper.toJson(status));
		out.flush();
		out.close();
	}
}
