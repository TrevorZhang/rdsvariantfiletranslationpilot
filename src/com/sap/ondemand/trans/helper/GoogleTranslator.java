/**
 * 
 */
package com.sap.ondemand.trans.helper;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Iterator;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 * @author I075006
 * 
 */
public class GoogleTranslator {

	/**
	 * 
	 * http://translate.google.cn/translate_a/t?client=p&sl=zh-CN&tl=ja&q=
	 * 
	 * @param term
	 * @param fromLang
	 * @param toLang
	 * @return
	 * @throws IOException
	 * @throws ParseException 
	 */
	public static String translate(String term, String fromLang, String toLang) throws IOException, ParseException {
		if("".equals(term)) {
			return "";
		}
		term = URLEncoder.encode(term, "utf-8");
		URL target = new URL("http://translate.google.com/translate_a/t?client=p&sl=" + fromLang + "&tl=" + toLang + "&ie=UTF-8&text=" + term);
		HttpURLConnection connection = (HttpURLConnection) target.openConnection();
		connection.setRequestMethod("GET");
		connection.setRequestProperty("User-Agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; InfoPath.1; CIBA");
		connection.setRequestProperty("Connection", "keep-alive");
		connection.setRequestProperty("Host", "translate.google.com");
		connection.setRequestProperty("Referer", "http://translate.google.com");
		
		connection.setUseCaches(true);
		connection.setDoOutput(true);
		connection.setDoInput(true);
		connection.setAllowUserInteraction(false);
		connection.connect();
		
		InputStream is = connection.getInputStream();
		BufferedInputStream bis = new BufferedInputStream(is);
		StringBuffer sb = new StringBuffer();
		byte[] read = new byte[4096];
		int length;
		while (-1 != (length = bis.read(read, 0, read.length))) {
			String str =new String(read, 0, length);
			sb.append(new String(str.getBytes(),"UTF-8"));
		}
		JSONObject json = (JSONObject)new JSONParser().parse(sb.toString());
		JSONArray sentences = (JSONArray)json.get("sentences");
		// take each value from the json array separately
		 Iterator<?> iterator = sentences.iterator();
		 String translation = "";
		 while (iterator.hasNext()) {
            JSONObject innerObj = (JSONObject) iterator.next();
            translation = (String) innerObj.get("trans");
		 }
		bis.close();
		is.close();
		return translation;
	}
		
}
