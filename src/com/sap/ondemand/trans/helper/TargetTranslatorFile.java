/**
 * 
 */
package com.sap.ondemand.trans.helper;

import java.io.InputStream;
import java.util.Iterator;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.apache.chemistry.opencmis.client.api.CmisObject;
import org.apache.chemistry.opencmis.client.api.Document;
import org.apache.chemistry.opencmis.client.api.ItemIterable;
import org.apache.chemistry.opencmis.commons.data.ContentStream;

import com.sap.ondemand.trans.jaxb.model.TranslationTasksModel;

/**
 * @author I075006
 *
 */
public class TargetTranslatorFile {
	
	public static TranslationTasksModel getMatchTasksModel(ItemIterable<CmisObject> iterator) {
		TranslationTasksModel taskModel = null;
		Iterator<CmisObject> iter = iterator.iterator();
		while (iter.hasNext()) {
			Object cmisObject = iter.next();
			if (cmisObject instanceof Document) {
				Document document = (Document) cmisObject;
				if(document.getName().endsWith(".xml") || document.getName().endsWith(".XML")) {
					ContentStream content = document.getContentStream();
					InputStream in2 = content.getStream();
					Unmarshaller jaxbUnmarshaller;
					try {
						JAXBContext jaxbContext = JAXBContext.newInstance(TranslationTasksModel.class);
						jaxbUnmarshaller = jaxbContext.createUnmarshaller();
						taskModel = (TranslationTasksModel) jaxbUnmarshaller.unmarshal(in2);
					} catch (JAXBException e) {
						e.printStackTrace();
					}
				}
			}
		}
		return taskModel;
	}
}
