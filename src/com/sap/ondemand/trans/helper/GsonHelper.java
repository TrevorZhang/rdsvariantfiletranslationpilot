/**
 * 
 */
package com.sap.ondemand.trans.helper;

import com.google.gson.Gson;

/**
 * @author I075006
 *
 */
public class GsonHelper {
	
	public static String toJson(Object jsonElement) {
		Gson gson = new Gson();
		return gson.toJson(jsonElement);
	}
}
