/**
 * 
 */
package com.sap.ondemand.trans.helper;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 * @author I075006
 *
 */
public class LocalHelper {
	
	/**
	 * 
	 * @return
	 */
	public static Map<String, String> getLocalInfo() {
		Map<String, String> localMap = new HashMap<String, String>();
		
		Locale[] locales = Locale.getAvailableLocales();
		for (Locale locale : locales) {
			localMap.put(locale.getDisplayName(), locale.getLanguage());
		}
		return localMap;
	}
}
