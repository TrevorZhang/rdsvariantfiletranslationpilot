/**
 * 
 */
package com.sap.ondemand.trans.helper;

import java.util.HashMap;
import java.util.Map;

import com.memetix.mst.language.Language;
import com.memetix.mst.translate.Translate;

/**
 * @author I075006
 *
 */
public class MicrosoftTranslationHelper {
	
	private static Map<String,Language> languageMap = new HashMap<String,Language>();
	
	static{
		Language[] languages = Language.values();
		for (Language language : languages) {
			if(!language.name().equals("AUTO_DETECT")) {
				languageMap.put(Language.valueOf(language.name()).toString(), Language.valueOf(language.name()));
			}
		}
	}
	
	public static String translator(String text, String from, String to) throws Exception {
		// Set your Windows Azure Marketplace client info - See http://msdn.microsoft.com/en-us/library/hh454950.aspx
	    Translate.setClientId("louisren");
	    Translate.setClientSecret("wS9Nsz98Enf21J6jFmfVti0YaFanD/lqAjzFTrYIMJU=");
	    return Translate.execute(text, languageMap.get(from), languageMap.get(to));
	}
	
//	public static List<LanguageModel> getLanguages() {
//		List<LanguageModel> languages = new ArrayList<LanguageModel>();
//		Set<String> keys = languageMap.keySet();
//		for (String language : keys) {
//			languages.add(new LanguageModel(language, true));
//		}
//		
//		return languages;
//	}
	
//	public static void main(String[] args) {
//		MicrosoftTranslationHelper.getLanguages();
//		String[] isoLanguages = Locale.getISOLanguages();
//		for (String string : isoLanguages) {
//			Locale locale = Locale.forLanguageTag(string);
//			
//			System.out.println("language===="+ locale +"/" + string);
//		}
//		
//	}
}
