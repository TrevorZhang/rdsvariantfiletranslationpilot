/**
 * 
 */
package com.sap.ondemand.trans.helper;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigInteger;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.Map;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.apache.chemistry.opencmis.client.api.CmisObject;
import org.apache.chemistry.opencmis.client.api.Document;
import org.apache.chemistry.opencmis.client.api.Folder;
import org.apache.chemistry.opencmis.client.api.ItemIterable;
import org.apache.chemistry.opencmis.client.api.QueryResult;
import org.apache.chemistry.opencmis.client.api.Session;
import org.apache.chemistry.opencmis.commons.PropertyIds;
import org.apache.chemistry.opencmis.commons.data.ContentStream;
import org.apache.chemistry.opencmis.commons.enums.UnfileObject;
import org.apache.chemistry.opencmis.commons.enums.VersioningState;
import org.apache.chemistry.opencmis.commons.exceptions.CmisBaseException;
import org.apache.chemistry.opencmis.commons.exceptions.CmisNameConstraintViolationException;
import org.apache.chemistry.opencmis.commons.exceptions.CmisObjectNotFoundException;
import org.apache.chemistry.opencmis.commons.impl.dataobjects.ContentStreamImpl;

import com.sap.ecm.api.EcmService;
import com.sap.ecm.api.RepositoryOptions;
import com.sap.ecm.api.RepositoryOptions.Visibility;
import com.sap.ondemand.trans.jaxb.model.ZipObjectModel;

/**
 * @author I075006
 * 
 */
public class CmisHelper {

	private static Session openCmisSession = null;
	private static String UNIQUE_NAME = "com.sap.hana.ondemand.translation";
	private static String UNIQUE_KEY = "cloud_translation_8080";

	public CmisHelper() {
		if (openCmisSession == null) {
			// Get a handle to the service by performing a JNDI lookup;
			// EcmService
			// must be a <resource-ref> in the web.xml
			EcmService ecmService = null;
			String ecmServiceLookupName = "java:comp/env/" + "EcmService";
			try {
				InitialContext ctx = new InitialContext();
				ecmService = (EcmService) ctx.lookup(ecmServiceLookupName);
			} catch (NamingException e) {
				throw new RuntimeException("Lookup of ECM service "
						+ ecmServiceLookupName
						+ " via JNDI failed with reason: " + e.getMessage());
			}

			// Create a key secured repository identified by a unique name and a
			// secret key (minimum length 10 characters)
			try {
				// Connect to ECM service accessing the repository
				openCmisSession = ecmService.connect(UNIQUE_NAME, UNIQUE_KEY);

			} catch (CmisObjectNotFoundException e) {
				// If the session couldn't be created the repository is missing
				// so
				// create it now
				RepositoryOptions options = new RepositoryOptions();
				options.setUniqueName(UNIQUE_NAME);
				options.setRepositoryKey(UNIQUE_NAME);
				options.setVisibility(Visibility.PROTECTED);
//				options.setMultiTenantCapable(true);
				ecmService.createRepository(options);

				// Now try again and connect to ECM service accessing the newly
				// created repository
				openCmisSession = ecmService.connect(UNIQUE_NAME, UNIQUE_KEY);
			} catch (Exception e) {
				throw new RuntimeException(
						"Connect to ECM service failed with reason: "
								+ e.getMessage());
			}
		}
	}

	/**
	 * Creates a folder. If the folder already exists nothing is done
	 * 
	 * @param session
	 *            the CMIS session
	 * @param rootFolder
	 *            the root folder of your repository
	 * @param folderName
	 *            the name of the folder to be created
	 * @return returns true if the folder has been created and returns false if
	 *         the folder already exists
	 */
	public Folder getFolderOfApp(String folderName) {
		Folder rootFolder = openCmisSession.getRootFolder();
		Folder appFolder = null;
		try {
			appFolder = (Folder) openCmisSession.getObjectByPath("/"
					+ folderName);
		} catch (CmisObjectNotFoundException e) {
			// Create the folder if it doesn't exist, yet
			Map<String, String> newFolderProps = new HashMap<String, String>();
			newFolderProps.put(PropertyIds.OBJECT_TYPE_ID, "cmis:folder");
			newFolderProps.put(PropertyIds.NAME, folderName);
			appFolder = rootFolder.createFolder(newFolderProps);
		}
		return appFolder;
	}

	public Folder getRootFolderOfRepository() {
		Folder rootFolder = openCmisSession.getRootFolder();
		return rootFolder;
	}

	/**
	 * Adds a document to the folder of your application
	 * 
	 * @param file
	 *            the file to be imported
	 */
	public void addDocument(File file, Folder folder) throws IOException {
		ContentStream contentStream =null;
		// create a new file in the root folder
		Map<String, Object> properties = new HashMap<String, Object>();
		properties.put(PropertyIds.OBJECT_TYPE_ID, "cmis:document");
		properties.put(PropertyIds.NAME, file.getName());
		// Try to identify the mime type of the file
		String mimeType = URLConnection.guessContentTypeFromName(file.getName());
		if (mimeType == null || mimeType.length() == 0) {
			mimeType = "application/octet-stream";
		}
		if (file != null && file.isFile()) {
			InputStream stream = new FileInputStream(file);
//			ContentStream contentStream = openCmisSession.getObjectFactory().createContentStream(file.getName(), file.length(), mimeType, stream);
			try {
				contentStream = new ContentStreamImpl(file.getName(), BigInteger.valueOf(file.length()), mimeType, stream);
				folder.createDocument(properties, contentStream, VersioningState.NONE);
			} catch (CmisNameConstraintViolationException e) {
				e.printStackTrace();
			} finally{
				if(stream !=null) {
					stream.close();
				}
			}
		}
	}

	// public void updateDoument(Document document, String fileName, byte[]
	// content)
	// throws FileNotFoundException, UnsupportedEncodingException {
	// InputStream stream = new ByteArrayInputStream(content);
	// ContentStream contentStream = new ContentStreamImpl(fileName,
	// BigInteger.valueOf(content.length),
	// "text/plain; charset=UTF-8", stream);
	// document.setContentStream(contentStream, true);
	// document.refresh();
	// }

	private Document getDocument(String documentName) throws Exception {
		ZipObjectModel objectModel = getDocumentProperties(documentName);
		return getDocumentById(objectModel.getObjectId());
	}

	private boolean documentExists(String documentName) {
		try {
			getDocument(documentName);
		} catch (CmisObjectNotFoundException e) {
			return false;
		} catch (Exception e) {
			return false;
		}

		return true;
	}

	public void deleteDocument(String documentName) throws Exception {
		Document documentToBeDeleted = getDocument(documentName);
		documentToBeDeleted.deleteAllVersions();
	}

	private Map<String, Object> getProperties(String documentName) {
        Map<String, Object> properties = new HashMap<String, Object>();
        properties.put(PropertyIds.OBJECT_TYPE_ID, "cmis:document");
        properties.put(PropertyIds.NAME, documentName);
        return properties;
    }
	
	private void createDocument(Folder folder, String documentName, byte[] documentContent) throws CmisNameConstraintViolationException {

        Map<String, Object> properties = getProperties(documentName);
        String mimeType = "text/plain; charset=UTF-8";
        ContentStream contentStream = getContentStream(documentName, mimeType, documentContent);
        folder.createDocument(properties, contentStream, VersioningState.NONE);
    }
	
	public void uploadDocument(Folder folder, String documentName, byte[] documentContent) throws Exception {
		if (documentExists(documentName)) {
			deleteDocument(documentName);
		}
		createDocument(folder, documentName, documentContent);
	}

	public void deleteDoument(String documentId) throws Exception {
		Document document = getDocumentById(documentId);
		document.delete(true);
	}

	/**
	 * 
	 * @param documentId
	 *            the document id of the file in the CMIS system
	 * @return The document as a CMIS Document object
	 * @throws Exception
	 */
	public Document getDocumentById(String documentId) throws Exception {
		try {
			Document doc = (Document) openCmisSession.getObject(documentId);
			return doc;
		} catch (CmisObjectNotFoundException onfe) {
			throw new Exception("Document doesn't exist!", onfe);
		} catch (CmisBaseException cbe) {
			throw new Exception("Could not retrieve the document:"
					+ cbe.getMessage(), cbe);
		}
	}
	
	public Folder getFolderById(String documentId) throws Exception {
		try {
			Folder doc = (Folder) openCmisSession.getObject(documentId);
			return doc;
		} catch (CmisObjectNotFoundException onfe) {
			throw new Exception("Document doesn't exist!", onfe);
		} catch (CmisBaseException cbe) {
			throw new Exception("Could not retrieve the document:"+ cbe.getMessage(), cbe);
		}
	}

	public File getPackage(String variantFile, String translatedFile)
			throws IOException {
		File zipCmisObject = null;
		ZipObjectModel zipObjectModel = getDocumentProperties(variantFile + ".zip");
		Document document;
		try {
			document = getDocumentById(zipObjectModel.getObjectId());
			InputStream inputStream = document.getContentStream().getStream();

			zipCmisObject = new File(document.getName());
			OutputStream os = new FileOutputStream(zipCmisObject);
			byte[] buffer = new byte[1024];
			int bytesRead;
			// read from is to buffer
			while ((bytesRead = inputStream.read(buffer)) != -1) {
				os.write(buffer, 0, bytesRead);
			}
			inputStream.close();
			os.flush();
			os.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return zipCmisObject;
	}

	public ContentStream getContentStream(String fileName, String mimeType,
			byte[] documentContent) {
		InputStream stream = new ByteArrayInputStream(documentContent);

		// String documentExtension =
		// this.name.substring(this.name.lastIndexOf('.') + 1);
		// String mimeType = "image/" + documentExtension;

		ContentStream contentStream = openCmisSession.getObjectFactory()
				.createContentStream(fileName, documentContent.length,
						mimeType, stream);

		return contentStream;
	}

	public void deleteAllDocument() {
		// Get root folder from CMIS session
		Folder rootFolder = getRootFolderOfRepository();
		// List content of root folder
		ItemIterable<CmisObject> children = rootFolder.getChildren();
		while (children.iterator().hasNext()) {
			for (CmisObject o : children) {
				if (o instanceof Folder) {
					Folder folder = (Folder) o;
					folder.deleteTree(true, UnfileObject.DELETE, true);
					// ItemIterable<CmisObject> folderItems =
					// folder.getChildren();
					// for (CmisObject cmisObject : folderItems) {
					// if (cmisObject instanceof Document) {
					// Document document = (Document) cmisObject;
					// document.delete();
					// }
					// }
					// folder.delete();
				} else if (o instanceof Document) {
					Document document = (Document) o;
					document.delete(true);
				}
			}
		}
	}

	public void deleteDocumentByDocumentId(String documentId) {
		Document document = (Document) openCmisSession.getObject(documentId);
		document.delete(true);
	}

	public ContentStream downloadDocumentByDocumentId(String documentId)
			throws IOException {
		Document document = (Document) openCmisSession.getObject(documentId);
		return document.getContentStream();
	}

	public ZipObjectModel getDocumentProperties(String fileName) {
		ZipObjectModel zipObjectModel = null;
		String query = "SELECT * FROM cmis:document WHERE cmis:name = '"
				+ fileName + "'";
		ItemIterable<QueryResult> queryResult = openCmisSession.query(query,
				false);
		for (QueryResult qr : queryResult) {
			zipObjectModel = new ZipObjectModel(qr
					.getPropertyByQueryName("cmis:objectId").getFirstValue()
					.toString(), qr
					.getPropertyByQueryName("cmis:contentStreamFileName")
					.getFirstValue().toString());
		}

		return zipObjectModel;
	}
	
	public ZipObjectModel getFolderProperties(String folderName) {
		ZipObjectModel zipObjectModel = null;
		String query = "SELECT * FROM cmis:folder WHERE cmis:name = '"+ folderName + "'";
		ItemIterable<QueryResult> queryResult = openCmisSession.query(query,false);
		for (QueryResult qr : queryResult) {
			zipObjectModel = new ZipObjectModel(qr.getPropertyByQueryName("cmis:objectId").getFirstValue().toString(), "");
		}

		return zipObjectModel;
	}

	public Document getDocumentByName(String documentName) {
		return (Document) openCmisSession.getObjectByPath("/" + documentName);
	}

	/**
	 * Connect to alfresco repository
	 * 
	 * @return root folder object
	 */
	// private static Folder connect() {
	// SessionFactory sessionFactory = SessionFactoryImpl.newInstance();
	// Map<String, String> parameter = new HashMap<String, String>();
	// parameter.put(SessionParameter.USER, "admin");
	// parameter.put(SessionParameter.PASSWORD, "admin");
	// // parameter.put(SessionParameter.ATOMPUB_URL, ALFRSCO_ATOMPUB_URL);
	// parameter.put(SessionParameter.BINDING_TYPE,
	// BindingType.ATOMPUB.value());
	// // parameter.put(SessionParameter.REPOSITORY_ID, REPOSITORY_ID);
	//
	// Session session = sessionFactory.createSession(parameter);
	// return session.getRootFolder();
	// }
}
