/**
 * 
 */
package com.sap.ondemand.trans.services;

/**
 * @author I075006
 *
 */
public class UploadFilesModel {

	private String 	fileName;
	private String 	referenceLanguage;
	private String 	targetLanguage;
	private String	image;
	
	/**
	 * @return the fileName
	 */
	public String getFileName() {
		return fileName;
	}
	/**
	 * @param fileName the fileName to set
	 */
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	/**
	 * @return the referenceLanguage
	 */
	public String getReferenceLanguage() {
		return referenceLanguage;
	}
	/**
	 * @param referenceLanguage the referenceLanguage to set
	 */
	public void setReferenceLanguage(String referenceLanguage) {
		this.referenceLanguage = referenceLanguage;
	}
	/**
	 * @return the targetLanguage
	 */
	public String getTargetLanguage() {
		return targetLanguage;
	}
	/**
	 * @param targetLanguage the targetLanguage to set
	 */
	public void setTargetLanguage(String targetLanguage) {
		this.targetLanguage = targetLanguage;
	}
	/**
	 * @return the image
	 */
	public String getImage() {
		return image;
	}
	/**
	 * @param image the image to set
	 */
	public void setImage(String image) {
		this.image = image;
	}
}
