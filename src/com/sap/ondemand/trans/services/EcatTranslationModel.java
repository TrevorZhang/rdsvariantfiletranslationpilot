/**
 * 
 */
package com.sap.ondemand.trans.services;

import java.util.Map;

import com.sap.ondemand.trans.jaxb.model.ParametersItem;

/**
 * @author I075006
 *
 */
public class EcatTranslationModel {

	private Map<String, ParametersItem> targetColumn;
	private Map<String, ParametersItem> preferenceColumn;
	
	/**
	 * @return the targetColumn
	 */
	public Map<String, ParametersItem> getTargetColumn() {
		return targetColumn;
	}
	/**
	 * @param targetColumn the targetColumn to set
	 */
	public void setTargetColumn(Map<String, ParametersItem> targetColumn) {
		this.targetColumn = targetColumn;
	}
	/**
	 * @return the preferenceColumn
	 */
	public Map<String, ParametersItem> getPreferenceColumn() {
		return preferenceColumn;
	}
	/**
	 * @param preferenceColumn the preferenceColumn to set
	 */
	public void setPreferenceColumn(Map<String, ParametersItem> preferenceColumn) {
		this.preferenceColumn = preferenceColumn;
	}
}
