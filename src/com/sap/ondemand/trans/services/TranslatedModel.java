/**
 * 
 */
package com.sap.ondemand.trans.services;

/**
 * @author I075006
 *
 */
public class TranslatedModel {
	
	private String fileName;
	private String modifyDate;
	
	public TranslatedModel() {
		
	}
	public TranslatedModel(String fileName, String modifyDate) {
		this.fileName = fileName;
		this.modifyDate =modifyDate;
	}
	
	/**
	 * @return the fileName
	 */
	public String getFileName() {
		return fileName;
	}
	/**
	 * @param fileName the fileName to set
	 */
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	/**
	 * @return the modifyDate
	 */
	public String getModifyDate() {
		return modifyDate;
	}
	/**
	 * @param modifyDate the modifyDate to set
	 */
	public void setModifyDate(String modifyDate) {
		this.modifyDate = modifyDate;
	}
}
