/**
 * 
 */
package com.sap.ondemand.trans.services;

/**
 * @author I075006
 *
 */
public class VariantFileModel {
	
	private String fileName;
	private String translated;
	private String translatedDate;
	private String typeImage;
	private String objType;
	
	/**
	 * @return the fileName
	 */
	public String getFileName() {
		return fileName;
	}
	/**
	 * @param fileName the fileName to set
	 */
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	/**
	 * @return the translated
	 */
	public String getTranslated() {
		return translated;
	}
	/**
	 * @param translated the translated to set
	 */
	public void setTranslated(String translated) {
		this.translated = translated;
	}
	/**
	 * @return the typeImage
	 */
	public String getTypeImage() {
		return typeImage;
	}
	/**
	 * @param typeImage the typeImage to set
	 */
	public void setTypeImage(String typeImage) {
		this.typeImage = typeImage;
	}
	/**
	 * @return the objType
	 */
	public String getObjType() {
		return objType;
	}
	/**
	 * @param objType the objType to set
	 */
	public void setObjType(String objType) {
		this.objType = objType;
	}
	/**
	 * @return the translatedDate
	 */
	public String getTranslatedDate() {
		return translatedDate;
	}
	/**
	 * @param translatedDate the translatedDate to set
	 */
	public void setTranslatedDate(String translatedDate) {
		this.translatedDate = translatedDate;
	}
}
