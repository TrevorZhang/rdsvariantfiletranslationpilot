/**
 * 
 */
package com.sap.ondemand.trans.files;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import javax.naming.NamingException;
import javax.xml.bind.JAXBException;

import org.apache.chemistry.opencmis.client.api.Folder;

import com.sap.ondemand.trans.helper.CmisHelper;
import com.sap.ondemand.trans.jaxb.JAXBHelper;
import com.sap.ondemand.trans.jaxb.model.Tasks;
import com.sap.ondemand.trans.jaxb.model.TasksItem;
import com.sap.ondemand.trans.jaxb.model.TranslationTasksModel;
/**
 * @author I075006
 *
 */
public class FileOperations {

	public static final String REGEX =  "_\\d\\s|_[A-Z]{2}\\s";
	public static Pattern PATTERN;
	private InputStream 			inputStream 			= null;
	private BufferedReader 			bufferedReader 			= null;
	private CmisHelper 				cmiseHelper				= null;
	private List<String>			filesList				= new ArrayList<String>();
	private static FileOperations singleton 				= new FileOperations();
	
	private FileOperations(){ }
	 
	/* Static 'instance' method */
	public static FileOperations getInstance( ) {
		return singleton;
	}
	   
	public boolean matcher(String row) {
		PATTERN 	= Pattern.compile(REGEX);
		Matcher m 	= PATTERN.matcher(row);
		boolean result = m.find();
		return result;
	}
	
	public void contentMatcher(File zipFile, Folder folder) throws IOException, NamingException, InstantiationException, IllegalAccessException, JAXBException{
		ZipFile zf = new ZipFile(zipFile, ZipFile.OPEN_READ);
		checkIsTranslationFile(zf, folder);
	}
	
	private void checkIsTranslationFile(ZipFile zf, Folder folder) throws InstantiationException, IllegalAccessException, IOException, NamingException, JAXBException {
		Enumeration<? extends ZipEntry> entries = zf.entries();
		while (entries.hasMoreElements()) {
			ZipEntry ze = entries.nextElement();
			if(!ze.isDirectory()) {
				if(ze.getName().contains(".xml") || ze.getName().contains(".XML")) {
					exportXmlFileFromZip(zf, ze, folder);
				}
			}
		}
	}
	
	private void exportXmlFileFromZip(ZipFile zf, ZipEntry ze, Folder folder) throws IOException, NamingException, InstantiationException, IllegalAccessException, JAXBException {
		String fileName = "";
		if(ze.getName().contains("/")) {
			fileName = ze.getName().substring(ze.getName().lastIndexOf("/")+1, ze.getName().length() );;
		} else {
			fileName = ze.getName();
		}
		File file = new File( fileName);
		inputStream 	= zf.getInputStream(ze);
		bufferedReader 	= new BufferedReader(new InputStreamReader(inputStream,"UTF-8"));
		PrintWriter out = new PrintWriter(file	,"UTF-8");
		String line = null;
		while((line = bufferedReader.readLine()) != null) {
			out.write(line + "\r\n");
			out.flush();
		}
		out.close();
		inputStream.close();
       
        TranslationTasksModel transModel = (TranslationTasksModel) JAXBHelper.onHeloper(file, TranslationTasksModel.class);
        Tasks tasks 			= transModel.getTasks();
        List<TasksItem> items 	= tasks.getItem();
        for (TasksItem tasksItem : items) {
        	filesList.add(tasksItem.getFileName());
		}
        
        if(cmiseHelper ==null) {
        	cmiseHelper =  new CmisHelper();
        }
        cmiseHelper.addDocument(file, folder);        
	}
}
