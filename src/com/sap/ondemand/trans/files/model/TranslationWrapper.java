package com.sap.ondemand.trans.files.model;

/**
 * 
 * @author I075006
 *
 */
public class TranslationWrapper {

	 private TranslationModel[] sentences;  
     private String src;  
     private String server_time;
	/**
	 * @return the sentences
	 */
	public TranslationModel[] getSentences() {
		return sentences;
	}
	/**
	 * @param sentences the sentences to set
	 */
	public void setSentences(TranslationModel[] sentences) {
		this.sentences = sentences;
	}
	/**
	 * @return the src
	 */
	public String getSrc() {
		return src;
	}
	/**
	 * @param src the src to set
	 */
	public void setSrc(String src) {
		this.src = src;
	}
	/**
	 * @return the server_time
	 */
	public String getServer_time() {
		return server_time;
	}
	/**
	 * @param server_time the server_time to set
	 */
	public void setServer_time(String server_time) {
		this.server_time = server_time;
	}
     
     
}
