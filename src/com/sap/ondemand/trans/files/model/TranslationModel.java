/**
 * 
 */
package com.sap.ondemand.trans.files.model;

/**
 * @author I075006
 *
 */
public class TranslationModel {
	 private String trans;  
     private String orig;  
     private String translit;  
     private String src_translit;
	/**
	 * @return the trans
	 */
	public String getTrans() {
		return trans;
	}
	/**
	 * @param trans the trans to set
	 */
	public void setTrans(String trans) {
		this.trans = trans;
	}
	/**
	 * @return the orig
	 */
	public String getOrig() {
		return orig;
	}
	/**
	 * @param orig the orig to set
	 */
	public void setOrig(String orig) {
		this.orig = orig;
	}
	/**
	 * @return the translit
	 */
	public String getTranslit() {
		return translit;
	}
	/**
	 * @param translit the translit to set
	 */
	public void setTranslit(String translit) {
		this.translit = translit;
	}
	/**
	 * @return the src_translit
	 */
	public String getSrc_translit() {
		return src_translit;
	}
	/**
	 * @param src_translit the src_translit to set
	 */
	public void setSrc_translit(String src_translit) {
		this.src_translit = src_translit;
	}
}
