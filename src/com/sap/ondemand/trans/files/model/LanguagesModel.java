/**
 * 
 */
package com.sap.ondemand.trans.files.model;

import java.util.List;

/**
 * @author I075006
 *
 */
public class LanguagesModel {
	
	private List<DocumentHeaderModel> relatives;
	private List<DocumentHeaderModel> needToTransColumn;
	private List<LanguageModel>		  languages;
	
	/**
	 * @return the relatives
	 */
	public List<DocumentHeaderModel> getRelatives() {
		return relatives;
	}
	/**
	 * @param relatives the relatives to set
	 */
	public void setRelatives(List<DocumentHeaderModel> relatives) {
		this.relatives = relatives;
	}
	/**
	 * @return the needToTransColumn
	 */
	public List<DocumentHeaderModel> getNeedToTransColumn() {
		return needToTransColumn;
	}
	/**
	 * @param needToTransColumn the needToTransColumn to set
	 */
	public void setNeedToTransColumn(List<DocumentHeaderModel> needToTransColumn) {
		this.needToTransColumn = needToTransColumn;
	}
	/**
	 * @return the languages
	 */
	public List<LanguageModel> getLanguages() {
		return languages;
	}
	/**
	 * @param languages the languages to set
	 */
	public void setLanguages(List<LanguageModel> languages) {
		this.languages = languages;
	}
}
