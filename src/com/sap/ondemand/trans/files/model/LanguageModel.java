/**
 * 
 */
package com.sap.ondemand.trans.files.model;

/**
 * @author I075006
 *
 */
public class LanguageModel {
	
	private String language;
	private boolean enabled;
	
	public LanguageModel(){
		
	}
	public LanguageModel(String language, boolean enabled) {
		this.language 	= language;
		this.enabled 	= enabled;
	}
	
	/**
	 * @return the language
	 */
	public String getLanguage() {
		return language;
	}

	/**
	 * @param language the language to set
	 */
	public void setLanguage(String language) {
		this.language = language;
	}
	
	/**
	 * @return the enabled
	 */
	public boolean isEnabled() {
		return enabled;
	}
	/**
	 * @param enabled the enabled to set
	 */
	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}
}
