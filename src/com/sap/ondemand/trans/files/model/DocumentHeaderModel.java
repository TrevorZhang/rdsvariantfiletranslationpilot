/**
 * 
 */
package com.sap.ondemand.trans.files.model;

/**
 * @author I075006
 *
 */
public class DocumentHeaderModel {
	
	private String headerName;
	private boolean enabled;
	
	public DocumentHeaderModel() {
		
	}
	public DocumentHeaderModel(String headerName, boolean enabled) {
		this.headerName	= headerName;
		this.enabled	= enabled;
	}
	/**
	 * @return the headerName
	 */
	public String getHeaderName() {
		return headerName;
	}
	/**
	 * @param headerName the headerName to set
	 */
	public void setHeaderName(String headerName) {
		this.headerName = headerName;
	}
	/**
	 * @return the enabled
	 */
	public boolean isEnabled() {
		return enabled;
	}
	/**
	 * @param enabled the enabled to set
	 */
	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}
}
